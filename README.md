# README #

### Development Info: ###

FE URL - https://dev.joobilix.com

BE Dev server - https://monolith.joobilix.com

The swagger UI info - https://monolith.joobilix.com/swagger


### Contribution guidelines ###

* Writing tests 
* Make sure the code is compatible with SOLID Principles
* Pull request
* Code review
* Update changes if required
* Marge

