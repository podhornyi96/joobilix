import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HostIndexComponent} from './layouts/host-layout/host-index.component';
import {HostProfileComponent} from './profile/host-profile/host-profile.component';
import {AuthGuard} from './auth.guard';
import {HomeComponent} from './home/home.component';
import {TermsOfUseComponent} from './shared/components/terms-of-use/terms-of-use.component';
import {PrivacyPolicyComponent} from './shared/components/privacy-policy/privacy-policy.component';
import {PersonalInfoComponent} from './profile/personal-info/personal-info.component';
import {CompanyIndexComponent} from './layouts/company-layout/company-index.component';
import {AboutComponent} from './profile/about/about.component';
import {CompanyProfileComponent} from './profile/company-profile/company-profile.component';
import {CreateEventComponent} from './events/create/create-event.component';
import {HostEventComponent} from './events/event/host-event.component';
import {HostListCompaniesComponent} from './companies/list-companies/host-list-companies.component';
import {PositionsComponent} from './positions/positions.component';
import {PositionComponent} from './positions/position/position.component';
import {ManageAccountsComponent} from './profile/manage-accounts/manage-accounts.component';
import {HomeGuard} from './home/home.guard';
import {EventNewInvitationComponent} from './events/event-new-invitation/event-new-invitation.component';
import {CandidateFullProfileComponent} from './events/candidates/candidate-profile/candidate-full-profile/candidate-full-profile.component';

//TODO: move host routes to separate host routing module.

const childHostRoutes: Routes = [
  {
    path: 'profile/edit',
    component: HostProfileComponent,
  },
  {
    path: 'profile',
    component: HostProfileComponent,
  },
  {
    path: 'event',
    component: CreateEventComponent
  },
  {
    path: 'event/:id',
    component: HostEventComponent,
  },
  {
    path: 'companies',
    component: HostListCompaniesComponent,
  },
];

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [HomeGuard]
  },
  {
    path: 'ChangePassword/:session',
    component: HomeComponent,
    data: {isOpenResetPassword: true}
  },
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'personal-info',
    component: PersonalInfoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'host',
    component: HostIndexComponent,
    children: childHostRoutes,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard]
  },
  {
    path: 'company',
    component: CompanyIndexComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {path: 'about', component: AboutComponent},
      {path: 'positions', component: PositionsComponent},
      {path: 'event/:id', component: HostEventComponent},
      {path: 'accounts', component: ManageAccountsComponent},
      {path: 'invitation/:id', component: EventNewInvitationComponent},
      {path: 'candidate/:id', component: CandidateFullProfileComponent}
    ]
  },
  {
    path: 'profile', children: [
      {path: 'company', component: CompanyProfileComponent, canActivate: [AuthGuard]}
    ]
  },
  {
    path: 'candidate/:id',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: CandidateFullProfileComponent},
  {
    path: 'position',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {path: '', component: PositionComponent},
      {
        path: ':positionId', children: [
          {path: '', component: PositionComponent},
          {path: 'event/:eventId', component: PositionComponent}
        ]
      },
      {path: 'event/:eventId', component: PositionComponent}
    ]
  },
  {
    path: 'home',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
