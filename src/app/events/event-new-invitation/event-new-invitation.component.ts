import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {EventService} from '../event.service';
import {EventModel} from '../event/event.model';
import {EventInteractionService} from '../../shared/services/interactions/event-interaction.service';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-event-new-invitation',
  templateUrl: './event-new-invitation.component.html',
  styleUrls: ['./event-new-invitation.component.less']
})
export class EventNewInvitationComponent implements OnInit {

  eventId: string;
  eventModel: EventModel;
  isSubmit = false;

  constructor(private activatedRout: ActivatedRoute,
              private eventService: EventService,
              private router: Router,
              private eventInteraction: EventInteractionService, private spinner: LoaderService) { }

  ngOnInit() {
    this.spinner.start();

    this.activatedRout.params.subscribe((params: Params) => {
      this.eventId = params['id'];

      this.initInvitation(this.eventId);
    });
  }

  locationFormatter(): string {
    if (this.eventModel && this.eventModel.location) {
      return `${this.eventModel.location.streetNumber} ${this.eventModel.location.street}, ${this.eventModel.location.city},
      ${this.eventModel.location.state}, ${this.eventModel.location.country}`;
    }
  }

  confirmInvitation() {
    if (this.isSubmit) {
      return;
    }

    this.isSubmit = true;
    this.eventService.invite(this.eventId, true)
      .subscribe(() => {
        this.isSubmit = false;
        this.eventInteraction.chooseNewInvitation(this.eventId);
        this.router.navigateByUrl(`/company/event/${this.eventId}`);
    });
  }

  onDeclined() {
    this.router.navigate(['/']);
  }

  private initInvitation(eventId: string): void {
    this.eventService.getById(eventId).subscribe((event: EventModel) => {
      this.eventModel = event;

      this.spinner.complete();
    });
  }

}
