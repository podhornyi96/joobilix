import {Component, OnInit} from '@angular/core';
import {CandidatesService} from '../../shared/candidates.service';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {CoreService} from '../../../../shared/services/core.service';
import {CandidateCvModel} from '../../shared/candidate-cv.model';
import {CandidateDetailsModel} from '../../shared/candidate-details.model';
import {EventModel} from '../../../event/event.model';
import {EventService} from '../../../event.service';

@Component({
  selector: 'jblx-candidate-full-profile',
  templateUrl: './candidate-full-profile.component.html',
  styleUrls: ['./candidate-full-profile.component.less']
})
export class CandidateFullProfileComponent implements OnInit {

  candidateDetails: CandidateDetailsModel;
  candidateId: string;
  candidateCv: CandidateCvModel;
  eventId: string;
  event: EventModel;

  constructor(
    private candidatesService: CandidatesService,
    private _activatedRoute: ActivatedRoute,
    public sanitizer: DomSanitizer,
    public coreService: CoreService,
    public eventService: EventService) {
    this.candidateDetails = new CandidateDetailsModel();
    this.candidateId = _activatedRoute.snapshot.paramMap.get('id');
    this.eventId = _activatedRoute.snapshot.queryParams['eventId'];
  }

  ngOnInit() {
    this.getCandidate();
    this.getEvent();
  }

  getCandidate() {
    if (!this.candidateId) {
      return;
    }
    this.candidatesService.getById(this.candidateId)
      .subscribe((result) => {
        this.candidateDetails = result;
        if (this.candidateDetails.hasCV && !this.coreService.isMobileUserAgent()) {
          this.getCv();
        }
      });
  }

  getEvent() {
    if (this.eventId === undefined || this.eventId === null) {
      return;
    }

    this.eventService.getById(this.eventId)
      .subscribe((result) => this.event = result);
  }

  getCv() {
    this.candidatesService.getCv(this.candidateId)
      .subscribe((result) => {
        this.candidateCv = result;
      });
  }
}
