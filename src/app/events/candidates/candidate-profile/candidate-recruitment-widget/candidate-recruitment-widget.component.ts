import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {CandidateDetailsModel} from '../../shared/candidate-details.model';

@Component({
  selector: 'jblx-candidate-recruitment-widget',
  templateUrl: './candidate-recruitment-widget.component.html',
  styleUrls: ['./candidate-recruitment-widget.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CandidateRecruitmentWidgetComponent implements OnInit {

  @Input() candidateDetails: CandidateDetailsModel;

  dataUpdated = false;
  rating = 0;
  comments = '';

  constructor() {
  }

  ngOnInit() {
  }

  onDebounce(val: any): void {
    this.dataUpdated = true;
  }

  save(): void {
    this.dataUpdated = true;
  }

}
