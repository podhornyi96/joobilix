import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {CandidateModel} from '../../shared/candidate.model';
import {CandidateDetailsModel, CourseInfo, VolunteerInfo} from '../../shared/candidate-details.model';
import {CandidatesService} from '../../shared/candidates.service';
import {EventModel, EventType} from '../../../event/event.model';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

@Component({
  selector: 'jblx-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CandidateProfileComponent {

  @Input() event: EventModel;
  @Input() eventId: string;

  @Output() candidateLoaded: EventEmitter<void> = new EventEmitter<void>();

  candidateDetails: CandidateDetailsModel;
  eventType = EventType;
  string = String;
  loading = false;

  scrollConfig: PerfectScrollbarConfigInterface = {
    wheelSpeed: 0.3
  };

  constructor(public candidatesService: CandidatesService) {
    this.candidateDetails = new CandidateDetailsModel();
  }

  load(candidate: CandidateModel): void {
    this.loading = true;

    this.candidatesService.getById(candidate.id).subscribe((cand: CandidateDetailsModel) => {
      this.candidateDetails = cand;

      //this.testCourses();
      //this.testVolunteers();

      this.loading = false;
      this.candidateLoaded.emit();
    });
  }

  private testCourses(): void {
    const course = new CourseInfo();

    course.gpa = 1;
    course.instituteName = 'Colman';
    course.graduatedDate = new Date();
    course.courseName = 'Software Engineering';

    this.candidateDetails.courseInfos = [
      course, course, course, course
    ];
  }

  private testVolunteers(): void {
    const vol = new VolunteerInfo();

    vol.description = 'Some description of this volunteer info';
    vol.organizationName = 'VP';
    vol.startYear = new Date().toString();
    vol.endYear = new Date().toString();

    this.candidateDetails.volunteerInfos = [
      vol, vol, vol, vol
    ];
  }

}
