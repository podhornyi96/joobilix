import {Component, Input, OnInit} from '@angular/core';
import {BachelorType, CandidateDegreeModel} from '../../shared/candidate-degree.model';
import {
  CandidateAvailabilityEnum, CourseInfo, ICandidateInfo, VolunteerInfo,
  YearsOfExperienceEnum
} from '../../shared/candidate-details.model';

import * as moment from 'moment';

@Component({
  selector: 'jblx-candidate-info',
  templateUrl: './candidate-info.component.html',
  styleUrls: ['./candidate-info.component.less']
})
export class CandidateInfoComponent implements OnInit {

  @Input() candidateDetails: ICandidateInfo;

  gpa: any = {
    0: '95-100',
    1: '90-95',
    2: '85-90',
    3: '80-85',
    4: '75-80',
    5: '70-75',
    6: 'Below 70',
    7: 'Confidential'
  };

  constructor() { }

  ngOnInit() {
  }

  superPowerFormatter() {
    const result = [this.candidateDetails.superPower1, this.candidateDetails.superPower2].filter(d => d).join(', ');
    return result;
  }

  // TODO: need description YearsOfExperienceEnum. we have -1 value what is it?
  experienceFormatter(): string {
    if (this.candidateDetails.yearsOfExperience === YearsOfExperienceEnum._0) {
      return 'No experience';
    }

    return `${this.candidateDetails.yearsOfExperience} Years of experience`;
  }

  // TODO: need description CandidateAvailabilityEnum. we have -1 value what is it?
  availabilityFormatter(): string {
    switch (this.candidateDetails.availability) {
      case CandidateAvailabilityEnum._0:
        return 'Summer semester';
      case CandidateAvailabilityEnum._1:
        return 'Semester 1';
      case CandidateAvailabilityEnum._2:
        return 'Semester 2';
      case CandidateAvailabilityEnum._sub_1:
        return 'Semester 1 Semester Summer semester';
      default:
        return '';
    }
  }

  educationFormatter(degree: CandidateDegreeModel): string {
    const rangeYear = this.yearRangeFormatter(degree.startDate, degree.graduatedDate);
    const degreeTitle = degree.bachelorType ? BachelorType[degree.bachelorType] : '';
    const faculty = degree.facultyName;
    const degreeGpa = degree.gpa ? this.gpa[degree.gpa] : '';

    const result = [rangeYear, degreeTitle, faculty, degree.instituteName, degreeGpa].filter(d => d).join(', ');
    return result;
  }

  courseFormatter(course: CourseInfo): string {
    const graduatedDate = course.graduatedDate ? this.formatDate(course.graduatedDate) : '';
    const courseGpa = course.gpa ? this.gpa[course.gpa] : '';

    const result = [graduatedDate, course.courseName, course.instituteName, courseGpa].filter(d => d).join(', ');
    return result;
  }

  volunteerFormatter(volunteer: VolunteerInfo): string {
    const startYear = volunteer.startYear ? this.formatDate(volunteer.startYear) : '';
    const endYear = volunteer.endYear ? this.formatDate(volunteer.endYear) : '';
    const rangeYear = startYear === endYear
      ? startYear : [startYear, endYear].filter(d => d).join('-');

    const result = [rangeYear, volunteer.organizationName, volunteer.description].filter(d => d).join(', ');
    return result;
  }

  private formatDate(date: string | Date): string {
    return moment(date).year().toString();
  }

  private yearRangeFormatter(start: string, end: string): string {
    let startEducation = '';
    let endEducation = '';

    if (start) {
      startEducation = this.formatDate(start);
    }

    if (end) {
      endEducation = this.formatDate(end);
    } else {
      return startEducation;
    }

    if (startEducation === endEducation) {
      return startEducation;
    }

    const result = [startEducation, endEducation].filter(d => d).join('-');
    return result;
  }
}
