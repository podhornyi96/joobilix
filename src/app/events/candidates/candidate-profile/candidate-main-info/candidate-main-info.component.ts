import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ICandidateMainInfo} from '../../shared/candidate-details.model';
import {CandidatesService} from '../../shared/candidates.service';
import {EventType} from '../../../event/event.model';
import {BaseCandidateComponent} from '../../shared/base-candidate.component';
import {InviteRequestModel} from '../../shared/invite-request.model';

@Component({
  selector: 'jblx-candidate-main-info',
  templateUrl: './candidate-main-info.component.html',
  styleUrls: ['./candidate-main-info.component.less']
})
export class CandidateMainInfoComponent implements OnInit, OnChanges {

  @Input() candidateDetails: ICandidateMainInfo;
  @Input() eventId: string;
  @Input() isFullProfile: boolean;
  @Input() currentEventType: EventType;

  applies: string[];
  private invited: boolean;
  private isGetting

  get isShowMeetBtn(): boolean {
    return this.currentEventType !== EventType.ON_GOING;
  }

  get isInvited(): boolean {
    return this.invited; // || this.candidateDetails.scheduled === ScheduledStatus.RequestToScheduled)
  }

  // TODO: one of the endpoints doesn't return property scheduled
  get isScheduled(): boolean {
    return false; // this.candidateDetails.scheduled === ScheduledStatus.RequestToScheduled)
  }

  private get isSchedule(): boolean {
    return this.currentEventType === EventType.SCHEDULING;
  }

  private get isInvite(): boolean {
    return this.currentEventType === EventType.RATE_AND_MATCH || this.currentEventType === EventType.VIEW_AND_APPLY;
  }

  constructor(private _cadidatesService: CandidatesService) {
    this.applies = [];
  }

  ngOnInit() {
    this.initial();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    const candidate = simpleChanges['candidateDetails'];
    if (candidate && !candidate.firstChange) {
      this.initial();
    }
  }

  profilePictureUrl(): string {
    const result = this.candidateDetails.profilePicUrl
      ? this.candidateDetails.profilePicUrl :
      '../../../../assets/images/candidate-default.png';

    return result;
  }

  fullName(): string {
    return `${this.candidateDetails.firstName} ${this.candidateDetails.lastName}`;
  }

  interests(): string {
    const result = [this.candidateDetails.fieldOfInterest1, this.candidateDetails.fieldOfInterest2]
      .filter(d => d).join(', ');
    return result;
  }

  getButtonText(): string {
    if (this.isInvited) {
      return 'Invited';
    }

    if (this.isInvite) {
      return 'Invite me';
    }

    if (this.isSchedule) {
      return 'Schedule';
    }

    return '';
  }

  // TODO: dublicate from BaseCandidateComponent need refactoring
  scheduleOrInvite(): void {
    if (this.isInvite) {
      this.invite();
    } else if (this.isSchedule) {
      this.schedule();
    }
  }

  // TODO: dublicate from BaseCandidateComponent need refactoring
  private invite(): void {
    this._cadidatesService.invite(this.candidateDetails.id, this.eventId).subscribe(() => {
      this.invited = true;
    });
  }

  // TODO: dublicate from BaseCandidateComponent need refactoring
  private schedule(): void {
    this._cadidatesService.schedule(this.candidateDetails.id).subscribe(() => {
      this.invited = true;
    });
  }

  private getApplies() {
    this._cadidatesService.getApplies(this.candidateDetails.id).subscribe((response) => {
      this.applies = response;
    });
  }

  private getInvites() {
    this._cadidatesService.getInvites(this.candidateDetails.id, this.eventId).subscribe((response) => {
      this.invited = response.length > 0;
    });
  }

  private initial() {
    if (this.candidateDetails) {
      this.getApplies();
      this.getInvites();
    }
  }
}
