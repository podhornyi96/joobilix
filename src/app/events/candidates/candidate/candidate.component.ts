import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CandidateModel, ScheduledStatus} from '../shared/candidate.model';
import * as moment from 'moment';
import {CandidatesService} from '../shared/candidates.service';
import {EventType} from '../../event/event.model';
import {BachelorType, CandidateDegreeModel} from '../shared/candidate-degree.model';
import {BaseCandidateComponent} from '../shared/base-candidate.component';

@Component({
  selector: 'jblx-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.less']
})
export class CandidateComponent extends BaseCandidateComponent implements OnInit {

  @Input() candidate: CandidateModel;
  
  @Output() candidateSelected: EventEmitter<CandidateModel> = new EventEmitter<CandidateModel>();

  string = String;
  eventType = EventType;
  scheduleType = ScheduledStatus;
  bachelorType = BachelorType;

  degree: CandidateDegreeModel;

  constructor(public candidatesService: CandidatesService) {
    super(candidatesService);
  }

  ngOnInit() {
    this.degree = this.getLongestPeriodDegree();
  }

  select(): void {
    setTimeout(() => {
      this.candidateSelected.emit(this.candidate);
    }, 50);
  }


  formatDate(date: string): number {
    return moment(date).year();
  }

  getButtonText(): string {
    if (this.candidate.invited || this.invited || this.candidate.scheduled === ScheduledStatus.RequestToScheduled) {
      return 'Invited';
    }

    if (this.event.eventType === EventType.RATE_AND_MATCH || this.event.eventType === EventType.VIEW_AND_APPLY) {
      return 'Invite';
    }

    return 'Schedule';
  }

  private getLongestPeriodDegree(): CandidateDegreeModel {
    return this.candidate.degrees.sort((a: CandidateDegreeModel, b: CandidateDegreeModel) => {
      return moment(a.graduatedDate).diff(moment(a.startDate), 'minutes') -
        moment(b.graduatedDate).diff(moment(b.startDate), 'minutes');
    })[0];
  }

}
