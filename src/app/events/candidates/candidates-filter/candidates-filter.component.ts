import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'jblx-candidates-filter',
  templateUrl: './candidates-filter.component.html',
  styleUrls: ['./candidates-filter.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CandidatesFilterComponent implements OnInit {

  someRange = [60, 100];

  constructor() { }

  ngOnInit() {
  }

}
