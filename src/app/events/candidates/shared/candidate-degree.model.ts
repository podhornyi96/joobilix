export class CandidateDegreeModel {
  id: string;
  gpa: GpaType;
  instituteName: string;
  facultyName: string;
  bachelorType: BachelorType;
  studingStatus: StudingStatus;
  startDate: string;
  graduatedDate: string;
}

export enum GpaType {
  Between_95_100 = 0,
  Between_90_95 = 1,
  Between_85_90 = 2,
  Between_80_85 = 3,
  Between_75_80 = 4,
  Between_70_75 = 5,
  Below_70 = 6,
  Confidential = 7
}

export enum StudingStatus {
  Graduated = 0,
  Studying = 1
}

export enum BachelorType {
  BA = 1,
  Bcom = 2,
  BCA = 3,
  BDS = 4,
  BEd = 5,
  BEng = 6,
  BTech = 7,
  BSc = 8,
  LLB = 9,
  MA = 10,
  MBA = 11,
  MBBCh = 12,
  BMBS = 13,
  BMBCh = 14,
  MBBS = 15,
  MSc = 16,
  PHD = 17,
  Other = 18
}
