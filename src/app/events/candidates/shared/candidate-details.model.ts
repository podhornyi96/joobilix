import {CandidateDegreeModel} from './candidate-degree.model';

export class CandidateDetailsModel implements ICandidateInfo, ICandidateMainInfo {
  id: string;
  firstName: string;
  lastName: string;
  profilePicUrl: string;
  fieldOfInterest1: string;
  fieldOfInterest2: string;
  yearsOfExperience: YearsOfExperienceEnum;
  degrees: CandidateDegreeModel[];
  courseInfos: CourseInfo[];
  superPower1: string;
  superPower2: string;
  volunteerInfos: VolunteerInfo[];
  hasCV: boolean;
  availability: CandidateAvailabilityEnum;
}

export class CourseInfo {
  courseName: string;
  instituteName: string;
  graduatedDate: Date;
  gpa: number;
}

export class VolunteerInfo {
  organizationName: string;
  description: string;
  startYear: string;
  endYear: string;
}

export interface ICandidateInfo {
  degrees: CandidateDegreeModel[];
  courseInfos: CourseInfo[];
  volunteerInfos: VolunteerInfo[];
  availability: CandidateAvailabilityEnum;
  yearsOfExperience: YearsOfExperienceEnum;
  superPower1: string;
  superPower2: string;
}

export interface ICandidateMainInfo {
  id: string;
  profilePicUrl: string;
  firstName: string;
  lastName: string;
  fieldOfInterest1: string;
  fieldOfInterest2: string;
}

export enum YearsOfExperienceEnum {
  _0 = 0,
  _1 = 1,
  _2 = 2,
  _3 = 3,
  _sub_1 = -1
}

export enum CandidateAvailabilityEnum {
  _0 = 0,
  _1 = 1,
  _2 = 2,
  _sub_1 = -1
}

