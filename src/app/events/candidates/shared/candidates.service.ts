import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {CandidateDetailsModel} from './candidate-details.model';
import {CandidateCommentModel} from './candidate-comment.model';
import {CandidateCvModel} from './candidate-cv.model';
import {CandidateModel} from './candidate.model';
import {InviteRequestModel} from './invite-request.model';

@Injectable({
  providedIn: 'root'
})
export class CandidatesService {

  constructor(private http: HttpClient) {
  }

  getById(candidateId: string): Observable<CandidateDetailsModel> {
    return this.http.get<CandidateDetailsModel>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}`);
  }

  comment(candidateId: string, model: CandidateCommentModel): Observable<void> {
    return this.http.put<void>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}/Comment`, model);
  }

  getCv(candidateId: string): Observable<CandidateCvModel> {
    return this.http.get<CandidateCvModel>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}/CV`);
  }

  getEventCandidates(eventId: string): Observable<CandidateModel[]> {
    return this.http.get<CandidateModel[]>(`${environment.apiUrl}/api/v2/Candidates?eventId=${eventId}`);
  }

  invite(candidateId: string, eventId: string): Observable<void> {
    const model = new InviteRequestModel();
    model.candidateId = candidateId;
    model.eventId = eventId;
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Candidates/Invite`, model);
  }

  schedule(candidateId: string): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}/Scheduled`, {});
  }

  getApplies(candidateId: string): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}/applies`, {});
  }

  getInvites(candidateId: string, eventId: string): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/v2/Candidates/${candidateId}/invites?eventId=${eventId}`, {});
  }
}
