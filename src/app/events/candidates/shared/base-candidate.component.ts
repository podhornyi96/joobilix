import {EventModel, EventType} from '../../event/event.model';
import {CandidatesService} from './candidates.service';
import {Input} from '@angular/core';
import {InviteRequestModel} from './invite-request.model';

export abstract class BaseCandidateComponent {

  @Input() event: EventModel;
  @Input() eventId: string;

  gpa: any = {
    0: '95-100',
    1: '90-95',
    2: '85-90',
    3: '80-85',
    4: '75-80',
    5: '70-75',
    6: 'Below 70',
    7: 'Confidential'
  };

  invited = false;

  protected constructor(public candidatesService: CandidatesService) {

  }

  abstract getButtonText(): string;

  scheduleOrInvite(candidateId: string): void {
    if (this.event.eventType === EventType.RATE_AND_MATCH || this.event.eventType === EventType.VIEW_AND_APPLY) {
      this.invite(candidateId);
    } else if (this.event.eventType === EventType.SCHEDULING) {
      this.schedule(candidateId);
    }
  }

  invite(candidateId: string): void {
    this.candidatesService.invite(candidateId, this.eventId).subscribe(() => {
      this.invited = true;
      // TODO: i think here we need to change button text and disable it. Need to know which status to set.
    });
  }

  schedule(candidateId: string): void {
    this.candidatesService.schedule(candidateId).subscribe(() => {
      this.invited = true;
      // TODO: i think here we need to change button text and disable it. Need to know which status to set.
    });
  }

}
