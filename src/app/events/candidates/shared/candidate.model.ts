import {CandidateDegreeModel} from './candidate-degree.model';

export interface CandidateModel {
  id: string;
  firstName: string;
  lastName: string;
  profilePicUrl: string;
  fieldOfInterest1: string;
  fieldOfInterest2: string;
  yearsOfExperience: number;
  degrees: CandidateDegreeModel[];
  rate: number;
  invited: boolean;
  scheduled: ScheduledStatus;
  hasCv: boolean;
}

export enum ScheduledStatus {
  None = 0,
  RequestToScheduled = 1,
  Scheduled = 2
}
