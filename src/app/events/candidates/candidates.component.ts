import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {CandidatesService} from './shared/candidates.service';
import {CandidateModel} from './shared/candidate.model';
import {EventModel} from '../event/event.model';
import {PageEvent} from '@angular/material';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {CandidateProfileComponent} from './candidate-profile/candidate-profile/candidate-profile.component';
import {SlidingPanelComponent} from '../../shared/components/sliding-panel/sliding-panel.component';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CandidatesComponent implements OnInit {

  @ViewChild('candidateProfile') candidateProfile: CandidateProfileComponent;
  @ViewChild('slidePanel') slidePanel: SlidingPanelComponent;

  @Input() eventId: string;
  @Input() event: EventModel;

  candidates: CandidateModel[] = [];
  paginationCandidates: CandidateModel[] = [];

  pageSize = 20;
  pageSizeOptions: number[] = [2, 5, 10, 20];

  refreshing = false;

  selectedCandidate: CandidateModel;

  constructor(private candidatesService: CandidatesService, private spinner: LoaderService, config: NgbDropdownConfig) {
    config.autoClose = false;
  }

  ngOnInit() {
    this.spinner.start();

    this.getAllCandidates();
  }

  onCandidateSelected(candidate: CandidateModel): void {
    this.selectedCandidate = candidate;

    if (this.selectedCandidate) {
      this.candidateProfile.load(candidate);
    }
  }

  refresh(): void {
    this.refreshing = true;

    this.getAllCandidates();
  }

  setPage(page: PageEvent): void {
    this.pageSize = page.pageSize;

    // TODO: I think here will be server side pagination. This will be removed
    this.paginationCandidates = this.candidates.slice(page.pageIndex * this.pageSize, page.pageIndex * this.pageSize + this.pageSize);

    window.scroll(0, 0);
  }

  private getAllCandidates(): void {
    this.candidatesService.getEventCandidates(this.eventId).subscribe((candidates: CandidateModel[]) => {
      this.candidates = candidates;
      // TODO: remove this. Just to test pagination
      // .concat(candidates).concat(candidates).concat(candidates).concat(candidates).concat(candidates).concat(candidates);


      this.paginationCandidates = this.candidates.slice(0, this.pageSize);

      this.refreshing = false;
      this.spinner.complete();
    });
  }

}
