import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EventModel} from './event/event.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {UpdateEventDataRequest} from './event/update-event-requrest.model';
import {CreateEventRequest} from './create/create-event.model';

@Injectable()
export class EventService {

  constructor(private http: HttpClient) {
  }

  getById(id: number | string): Observable<EventModel> {
    return this.http.get<EventModel>(`${environment.apiUrl}/api/v2/Events/${id}`);
  }

  update(eventId: string, event: UpdateEventDataRequest): Observable<void> {
    return this.http.put<void>(`${environment.apiUrl}/api/v2/Events/${eventId}`, event);
  }

  create(createEventRequest: CreateEventRequest): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Host/Event`, createEventRequest);
  }

  createPosition(eventId: string, positionId: string): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Events/${eventId}/Positions/${positionId}`, {});
  }

  deleteEventPosition(eventId: string, positionId: string): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/api/v2/Events/${eventId}/Positions/${positionId}`);
  }

  invite(eventId: string, accept: boolean, reason: string = ''): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Events/${eventId}/Invitation`, {
      accept: accept,
      reason: reason
    });
  }

}
