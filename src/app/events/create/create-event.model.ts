export class CreateEventRequest{
  name: string;
  when: string;
  endDate: string;
}
