import {Component, OnInit, ViewChild} from '@angular/core';
import {CreateEventRequest} from './create-event.model';
import {INgxMyDpOptions, NgxMyDatePickerDirective} from 'ngx-mydatepicker';
import * as moment from 'moment';
import {DateHelper} from '../../shared/helpers/dateHelper';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {EventService} from '../event.service';

@Component({
  selector: 'create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.less']
})
export class CreateEventComponent implements OnInit {

  @ViewChild('dp') ngxdp: NgxMyDatePickerDirective;
  @ViewChild('createEventForm') createEventForm: NgForm;

  createEventRequest: CreateEventRequest = new CreateEventRequest();

  from: string;
  to: string;

  eventSaved = false;

  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  };

  hoursAndMinutesMask: any = {
    mask: [/\d/, /\d/, ':', /\d/, /\d/],
  };

  private date: any = {};

  get dateData(): any {
    return this.date;
  }

  set dateData(value: any) {
    if (!value) {
      return;
    }

    this.date = value.jsdate;

    this.createEventRequest.when = DateHelper.setMinutesAndHours(moment(this.date).format('MM/DD/YYYY HH:mm'), this.from);
    this.createEventRequest.endDate = DateHelper.setMinutesAndHours(moment(this.date).format('MM/DD/YYYY HH:mm'), this.to);
  }

  constructor(private eventService: EventService) {
  }

  ngOnInit() {

  }

  get startDateData(): string {
    return this.from;
  }

  set startDateData(value: string) {
    if (!value) {
      this.from = null;
      return;
    }

    if (!DateHelper.isValidHoursAndMinutes(value)) {
      return;
    }

    this.from = value;

    this.createEventRequest.when = DateHelper.setMinutesAndHours(this.createEventRequest.when, value);
  }

  get endDateData(): string {
    return this.to;
  }

  set endDateData(value: string) {
    if (!value) {
      this.to = null;
      return;
    }

    if (!DateHelper.isValidHoursAndMinutes(value)) {
      return;
    }

    this.to = value;

    this.createEventRequest.endDate = DateHelper.setMinutesAndHours(this.createEventRequest.endDate, value);
  }

  onSubmit(): void {
    if (!FormHelper.isFormValid(this.createEventForm) || !this.ngxdp.isDateValid()) {
      return;
    }

    this.eventService.create(this.createEventRequest).subscribe(resp => {
      this.eventSaved = true;
    });
  }

}
