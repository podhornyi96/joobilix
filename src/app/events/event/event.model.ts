import {EventStatusEnum} from '../../shared/models/host/event/host-event.model';
import {RegistrationStatusEnum} from '../../shared/models/host/company/host-event-company.model';

export class EventModel {
  id: number;
  name: string;
  when: string;
  endDate: string;
  eventType: EventType;
  description: string;
  code: string;
  closeEvent: boolean;
  eventStatus: EventStatusEnum;
  location: LocationModel;
  participant: boolean;
  host: boolean;
  registrationStatus: RegistrationStatusEnum;
}

export enum EventType {
  ON_GOING = 0,
  VIEW_AND_APPLY = 1,
  SCHEDULING = 2,
  RATE_AND_MATCH = 3
}

export class LocationModel {
  longitude: number;
  latitude: number;
  country: string;
  state: string;
  city: string;
  street: string;
  streetNumber: string;
  placeName: string;
}
