import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {EventService} from '../event.service';
import {EventModel} from './event.model';
import {UpdateEventDataRequest} from './update-event-requrest.model';
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import {EventDescriptionComponent} from '../description/event-description.component';
import {ObjectHelper} from '../../shared/helpers/objectHelper';
import {LoaderService} from '../../shared/loader/loader.service';
import {RegistrationStatusEnum} from '../../shared/models/host/company/host-event-company.model';

@Component({
  selector: 'jblx-host-event',
  templateUrl: './host-event.component.html',
  styleUrls: ['./host-event.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class HostEventComponent implements OnInit {

  @ViewChild('descriptionComponent') eventDescriptionComponent: EventDescriptionComponent;

  originalUpdateRequest: UpdateEventDataRequest = new UpdateEventDataRequest();
  currentUpdateRequest: UpdateEventDataRequest = new UpdateEventDataRequest();
  eventModel: EventModel;
  eventId: string;
  editEnabled = false;
  tabId: string;

  constructor(private route: ActivatedRoute, private router: Router, private eventService: EventService,
              private spinner: LoaderService) {
  }

  ngOnInit() {
    this.spinner.start();
    this.tabId = this.route.snapshot.queryParams['tab'];

    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];

      this.initEvent(this.eventId);
    });
  }

  private initEvent(eventId: string): void {
    this.eventService.getById(eventId).subscribe((event: EventModel) => {
      
      if (event && event.registrationStatus === RegistrationStatusEnum.inviteSent) {
        this.router.navigateByUrl(`/company/invitation/${this.eventId}`);
      }

      this.eventModel = event;

      this.spinner.complete();

      this.initObjects(event);
    });
  }

  toggle(tabsSet: NgbTabset): void {
    if (this.getTabId(tabsSet.activeId) === 3 && !this.eventDescriptionComponent.isFormValid()) {
      return;
    }


    if (this.editEnabled && !ObjectHelper.areObjectEquals(this.currentUpdateRequest, this.originalUpdateRequest)) {

      this.eventService.update(this.eventId, this.currentUpdateRequest).subscribe(response => {
        this.eventService.getById(this.eventId).subscribe((event: EventModel) => {
          this.initObjects(event);
        });
      });
    }

    this.editEnabled = !this.editEnabled;
  }

  mapEvent(event: EventModel): UpdateEventDataRequest {
    return new UpdateEventDataRequest({
      code: event.code,
      description: event.description,
      endDate: event.endDate,
      location: event.location,
      when: event.when,
      name: event.name,
      closeEvent: event.closeEvent,
      eventStatus: event.eventStatus
    });
  }

  getTabId(value: string): number {
    if (!value) {
      return null;
    }

    return parseInt(value.substring(value.lastIndexOf('_') + 1), null);
  }

  onSelectedTab(tab) {
    this.editEnabled = false;
    this.router.navigate([], { queryParams: { tab: tab.nextId }});
  }

  private initObjects(event: EventModel): void {
    const respnoseEvent: UpdateEventDataRequest = this.mapEvent(event);
    this.originalUpdateRequest = respnoseEvent;
    this.currentUpdateRequest = JSON.parse(JSON.stringify(respnoseEvent));
  }
}
