import {LocationModel} from './event.model';
import {EventStatusEnum} from '../../shared/models/host/event/host-event.model';

export class UpdateEventDataRequest {
  name: string;
  when: string;
  endDate: string;
  location: LocationModel;
  description: string;
  closeEvent: boolean;
  code: string;
  eventStatus: EventStatusEnum;


  constructor(obj?: Partial<UpdateEventDataRequest>) {
    Object.assign(this, obj);
  }

}
