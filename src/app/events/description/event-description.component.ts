import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {LocationModel} from '../event/event.model';

import * as moment from 'moment';
import {DateCompareOperation} from '../../shared/validators/date-compare.validator';
import {UpdateEventDataRequest} from '../event/update-event-requrest.model';
import {NgForm, Validators} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';

@Component({
  selector: 'event-description',
  templateUrl: './event-description.component.html',
  styleUrls: ['./event-description.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class EventDescriptionComponent implements OnInit {

  @ViewChild('eventDescriptionForm') eventDescriptionForm: NgForm;

  @Input() event: UpdateEventDataRequest;

  @Input() editEnabled: boolean;
  @Input() isHost: boolean;

  userSettings = {
    showRecentSearch: false,
    showCurrentLocation: false,
    showSearchButton: false,
    inputPlaceholderText: 'location',
    inputString: ''
  };

  private startDate: string;
  private endDate: string;

  operation = DateCompareOperation;

  whenDateMask: any = {
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/]
  };

  endDateMask: any = {
    mask: [/\d/, /\d/, ':', /\d/, /\d/]
  };

  constructor() {
  }

  ngOnInit() {
    this.startDate = moment(this.event.when).format('HH:mm');
    this.endDate = moment(this.event.endDate).format('HH:mm');

    this.userSettings.inputString = this.event.location
    && !String.isNullOrEmpty(this.event.location.placeName) ? this.event.location.placeName : '';

    // according to documentation to update location
    this.userSettings = Object.assign({}, this.userSettings);
  }

  isFormValid(): boolean {
    return FormHelper.isFormValid(this.eventDescriptionForm);
  }

  toggleCode(): void {
    this.event.closeEvent = !this.event.closeEvent;

    this.setCodeValidators(this.event.closeEvent);
  }

  autoCompleteCallback1(selectedData: any) {
    if (!selectedData.response)
      return;

    this.event.location = new LocationModel();

    this.event.location.latitude = selectedData.data.geometry.location.lat;
    this.event.location.longitude = selectedData.data.geometry.location.lng;

    this.event.location.placeName = selectedData.data.formatted_address;

    for (let addressComponent of selectedData.data.address_components) {
      if (addressComponent.types.indexOf('country') !== -1) {
        this.event.location.country = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('locality') !== -1) {
        this.event.location.city = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('administrative_area_level_1') !== -1) {
        this.event.location.state = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('route') !== -1) {
        this.event.location.street = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('street_number') !== -1) {
        this.event.location.streetNumber = addressComponent.long_name;
      }
    }
  }

  get whenDateData(): string {
    if (!this.event.when || !moment(this.event.when).isValid())
      return;

    return moment(this.event.when).format('MM/DD/YY');
  }

  set whenDateData(value: string) {
    if (!value) {
      this.event.when = null;
      this.event.endDate = null;
      return;
    }

    if (!moment(value).isValid())
      return;

    this.event.when = moment(value).format('MM/DD/YYYY HH:mm');
    this.event.endDate = moment(value).format('MM/DD/YYYY HH:mm');
  }

  get startDateData() {
    if (!this.event.when || !moment(this.event.when).isValid())
      return;

    return this.startDate;
  }

  set startDateData(value: string) {
    if (!value || !this.event.when || !moment(this.event.when).isValid()) {
      this.startDate = null;

      return;
    }

    const hoursAndMinutes: string[] = value.split(':');

    if (!this.isValidMinutesAndHours(value))
      return;

    const date = moment(this.event.when);

    date.set('hour', +hoursAndMinutes[0]);
    date.set('minute', +hoursAndMinutes[1]);

    this.event.when = date.format('MM/DD/YYYY HH:mm');
  }

  get endDateData() {
    if (!this.event.endDate || !moment(this.event.endDate).isValid())
      return;

    return this.endDate;
  }

  set endDateData(value: string) {
    if (!value) {
      // this.event.endDate = null;

      return;
    }

    const hoursAndMinutes: string[] = value.split(':');

    if (!this.isValidMinutesAndHours(value))
      return;

    const date = moment(this.event.when);

    date.set('hour', +hoursAndMinutes[0]);
    date.set('minute', +hoursAndMinutes[1]);

    this.event.endDate = date.format('MM/DD/YYYY HH:mm');
  }

  getFormattedDate(date: Date, format: string): string {
    return moment(date).format(format);
  }

  private isValidMinutesAndHours(value: string): boolean {
    const hoursAndMinutes: string[] = value.split(':');

    if (hoursAndMinutes.length !== 2 || isNaN(parseInt(hoursAndMinutes[0])) || isNaN(parseInt(hoursAndMinutes[1])) || value.indexOf('_') !== -1) {
      return false;
    }

    return true;
  }

  private setCodeValidators(closeEvent: boolean): void {
    if (closeEvent) {
      this.eventDescriptionForm.controls['eventCode'].setValidators([Validators.required]);
    } else {
      this.eventDescriptionForm.controls['eventCode'].setValidators([]);
    }
  }

}
