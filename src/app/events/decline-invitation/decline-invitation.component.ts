import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EventService} from '../event.service';

@Component({
  selector: 'jblx-decline-invitation',
  templateUrl: './decline-invitation.component.html',
  styleUrls: ['./decline-invitation.component.less']
})
export class DeclineInvitationComponent implements OnInit {

  @ViewChild('content') modal: ElementRef;

  @Input() eventId: string;
  @Output() declined: EventEmitter<void> = new EventEmitter<void>();

  modalReference: NgbModalRef;

  reason: string;

  constructor(private modalService: NgbModal, private eventService: EventService) {
  }

  ngOnInit() {
  }

  show(): void {
    this.modalReference = this.modalService.open(this.modal, {size: 'lg', backdrop: 'static'});
  }

  onSubmit(): void {
    this.eventService.invite(this.eventId, false, this.reason).subscribe(() => {
      this.modalReference.close();
      this.declined.emit();
    });
  }
}
