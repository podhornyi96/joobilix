import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventStatusEnum, HostEvent} from '../../shared/models/host/event/host-event.model';
import {Subject} from 'rxjs/Rx';
import {DOCUMENT} from '@angular/common';
import {PageScrollInstance, PageScrollService} from 'ngx-page-scroll';

import * as moment from 'moment';
import {WindowRef} from '../../shared/services/window-ref';
import {HostService} from '../../host/host.service';

@Component({
  selector: 'jblx-event-slider',
  templateUrl: './event-slider.component.html',
  styleUrls: ['./event-slider.component.less']
})
export class EventSliderComponent implements OnInit, OnDestroy {

  @ViewChild('container')
  private container: ElementRef;

  events: HostEvent[];
  eventStatus = EventStatusEnum;
  isStart = true;
  isEnd = false;
  isSlider = true;

  private pageScrollDuration = 200;
  private unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private stepHostEventLoad = 50;

  constructor(private _hostApiService: HostService,
              private pageScrollService: PageScrollService,
              @Inject(DOCUMENT) private document: any,
              private winRef: WindowRef) {
    this.events = [];
    this.winRef.nativeWindow.onload = () => {
      setTimeout(() => { this.isSlider =
      this.container.nativeElement.scrollWidth >
      this.container.nativeElement.offsetWidth;
      }, 1500);
    };
  }

  ngOnInit() {
    this.getAllEvents();
  }

  getAllEvents(skip: number = 0, limit: number = 50, prevRequest: HostEvent[] = []) {
    if (skip === 0) {
      this.events = [];
    }

    this._hostApiService.getHostEvents(skip, limit)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data.length) {
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepHostEventLoad;
          this.getAllEvents(skip, limit, prevRequest);
        } else {
          let currentDate = moment().set({hour: 0, minute: 0, second: 0});
          this.events = prevRequest.filter(d => d.eventStatus === this.eventStatus.creatingCompleted
            && moment(d.when).isAfter(currentDate))
            .sort((left, right) => {
              return left.when.valueOf() - right.when.valueOf();
            });
        }
      });
  }

  next(): void {
    if (this.isEnd) {
      return;
    }

    this.offsetScroll(true);
  }

  prev(): void {
    if (this.isStart) {
      return;
    }

    this.offsetScroll(false);
  }

  offsetScroll(isDirect: boolean): void {
    let target = this.getScrollOffsetParam(isDirect);
    let pageScrollInstance: PageScrollInstance = PageScrollInstance.newInstance({
      document: this.document,
      scrollTarget: target.id,
      pageScrollOffset: target.offset,
      verticalScrolling: false,
      pageScrollDuration: this.pageScrollDuration,
      scrollingViews: [this.container.nativeElement]});
    this.pageScrollService.start(pageScrollInstance);
    this.checkScrollPosition();
  }

  private getScrollOffsetParam(isDirect: boolean): {id: string, offset: number} {
    let k = isDirect ? 1 : -1;
    if (!isDirect && this.container.nativeElement.scrollLeft < 300) {
      return {id: '#event_1', offset: 400};
    }

    let index = Math.floor(this.container.nativeElement.scrollLeft / 200) + k;
    return {id: '#event_' + index, offset: 70};
  }

  private checkScrollPosition() {
    let timeout = this.pageScrollDuration + 10;
    setTimeout(() => {
      this.isStart = this.container.nativeElement.scrollLeft <= 0;
      this.isEnd = (this.container.nativeElement.offsetWidth + this.container.nativeElement.scrollLeft) >=
        (this.container.nativeElement.scrollWidth - 30);
      }, timeout);
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
