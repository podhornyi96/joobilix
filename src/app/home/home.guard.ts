import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CurrentProfile} from '../account/current-profile.model';
import {AccountService} from '../account/account.service';
import {RoleConstants} from '../shared/constants/role.constants';
import {RedirectHelper} from '../shared/helpers/redirectHelper';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {

  constructor(private accountService: AccountService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.accountService.isLoggedIn()) {
      return true;
    }

    return this.accountService.getCurrentProfile().toPromise().then((currentProfile: CurrentProfile) => {
      const nextUrl = this.getUrlFromRouter(next);
      if (!this.accountService.isLoginStepsDone(nextUrl)) {
        const redirectUrl = RedirectHelper.getLoginFlowRedirectUrl(currentProfile);

        this.router.navigate([redirectUrl]);

        return Promise.resolve(false);
      }

      if (currentProfile.roles.indexOf(RoleConstants.Host) !== -1) {
        this.router.navigate(['/host/profile']);
      } else {
        this.router.navigate(['/company/about']);
      }

      // here can be more variants where to redirect

      return Promise.resolve(true);
    });
  }

  private getUrlFromRouter(router: ActivatedRouteSnapshot): string {
    const result = router.pathFromRoot
      .filter(p => p.routeConfig && p.routeConfig.path)
      .map(p => p.routeConfig.path).join('/');
    return result;
  }
}
