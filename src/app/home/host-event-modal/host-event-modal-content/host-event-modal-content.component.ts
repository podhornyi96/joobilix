import {Component, OnInit, ViewEncapsulation, ViewChild, Output, EventEmitter} from '@angular/core';
import {HostEventRequest} from '../host-event-request.model';
import {NgForm} from '@angular/forms';
import {HostEventResponse} from '../host-event-response.model';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import {HostService} from '../../../host/host.service';
import {FormHelper} from '../../../shared/helpers/formHelper';

@Component({
  selector: 'jblx-host-event-modal-content',
  templateUrl: './host-event-modal-content.component.html',
  styleUrls: ['./host-event-modal-content.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class HostEventModalContentComponent implements OnInit {

  @ViewChild(NgForm) hostEventForm: NgForm;

  @Output() modalClosed: EventEmitter<void> = new EventEmitter<void>();

  hostEventRequest: HostEventRequest = new HostEventRequest();

  submitted = false;
  registered = false;

  phoneTextMask: any = {
    mask: ['+', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    guide: false
  };

  captchaKey = environment.hostEventCapchaKey;

  constructor(private hostService: HostService, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  onSubmit(): void {
    this.submitted = true;

    if (!FormHelper.isFormValid(this.hostEventForm)) {
      return;
    }

    this.hostService.hostEvent(this.hostEventRequest).subscribe((response: HostEventResponse) => {

      if (!response.capchaFailed) {
        this.registered = true;
      } else {
        this.toastr.error('An error occurred! ');
      }

    });
  }

  closeModal(): void {
    this.modalClosed.next();
  }

}
