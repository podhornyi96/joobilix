
export class HostEventRequest {
    firstName: string;
    lastName: string;
    organization: string;
    phoneNumber: string;
    emailAddress: string;
    eventDate: string;
    capchaSession: string;
}