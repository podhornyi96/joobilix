import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HostEventModalContentComponent } from './host-event-modal-content/host-event-modal-content.component';

@Component({
  selector: 'host-event-modal',
  template: ''
})
export class HostEventModalComponent implements OnInit {

  private modalRef: NgbModalRef;

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  openModal() {
    this.modalRef = this.modalService.open(HostEventModalContentComponent, { size: 'lg'});

    this.modalRef.componentInstance.modalClosed.subscribe(x => {
      this.modalRef.close();
    });
  }

}
