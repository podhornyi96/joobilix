import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NguCarousel} from '@ngu/carousel';
import {LoginComponent} from '../account/login/login.component';

@Component({
  selector: 'jblx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements AfterViewInit {

  @ViewChild('loginModal') loginModal: LoginComponent;
  isOpenResetPassword: boolean;

  public carouselOne: NguCarousel = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
    slide: 1,
    speed: 400,
    interval: 4000,
    point: {
      visible: false
    },
    touch: true,
    loop: true,
  };

  isCollapsed = true;

  constructor(private route: ActivatedRoute) {
    this.isOpenResetPassword = route.snapshot.data['isOpenResetPassword'];
  }

  ngAfterViewInit(): void {
    const openLogin: boolean = ('true' == this.route.snapshot.queryParams['openLogin']);

    if (openLogin) {
      setTimeout(() => {
        this.loginModal.openModal();
      }, 500);
    }
  }

}
