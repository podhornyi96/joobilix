import {ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SideModalComponent} from '../../shared/modals/side-modal/side-modal.component';
import {PositionSummary} from './models/position-summary';
import {PositionsTableData} from './models/positions-table-data';

@Component({
  selector: 'jblx-positions-table',
  templateUrl: './positions-table.component.html',
  styleUrls: ['./positions-table.component.less'],
})
export class PositionsTableComponent implements OnInit {

  @Input() data: PositionsTableData;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('sideModal') sideModal: SideModalComponent;

  model: MatTableDataSource<PositionSummary>;
  displayedColumns: string[];
  pageSize = [10];

  constructor() {
    this.displayedColumns = ['title', 'field', 'open', 'numberOfApplies', 'actions'];
  }

  removeRow(positionId: string): void {
    const position = this.data.positions.find((pos) => pos.id === positionId);

    if (!position) {
      return;
    }

    const index = this.data.positions.indexOf(position);

    this.data.positions.splice(index, 1);

    // this is bypass for material table to refresh it
    this.model.data = this.model.data;
  }

  ngOnInit() {
    this.initMatTable();
  }

  applyFilter(filterValue: string) {
    this.model.filter = filterValue.trim().toLowerCase();
  }

  initMatTable() {
    this.model = new MatTableDataSource<PositionSummary>(this.data.positions);
    this.initPaginator();
    this.model.sort = this.sort;
  }

  initPaginator() {
    if (!this.model) {
      return;
    }

    this.model.paginator = this.paginator;
  }

  statusFormatter(isOpen: boolean): string {
    return isOpen ? 'open' : 'hired';
  }

  numberOfAppliesFormatter(count: number) {
    const phrase = count === 1 ? 'candidate' : 'candidates';
    return `${count} ${phrase}`;
  }


}
