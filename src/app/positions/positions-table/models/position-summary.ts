export class PositionSummary {
  id: string;
  title: string;
  field: string;
  open: boolean;
  numberOfApplies: number;
}
