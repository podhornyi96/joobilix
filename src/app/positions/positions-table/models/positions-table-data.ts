import {PositionSummary} from './position-summary';

export class PositionsTableData {
  positions: PositionSummary[];
  actions: PositionTableAction[];
  onClickRowHandler: (model: PositionSummary) => void;
}

export class PositionTableAction {
  title: string;
  action: (data: PositionSummary) => void;
}
