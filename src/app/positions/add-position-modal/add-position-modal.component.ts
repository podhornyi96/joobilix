import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {PositionService} from '../position.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PositionAutocompleteModel} from './position-autocomplete.model';
import {EventService} from '../../events/event.service';

@Component({
  selector: 'jblx-add-position-modal',
  templateUrl: './add-position-modal.component.html',
  styleUrls: ['./add-position-modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AddPositionModalComponent implements OnInit {

  @ViewChild('content') modal: ElementRef;

  modalReference: NgbModalRef;

  selectedPosition: PositionAutocompleteModel;

  positions: PositionAutocompleteModel[] = [];

  eventId: string;

  constructor(private route: ActivatedRoute, private router: Router, private modalService: NgbModal,
              private positionService: PositionService, private eventService: EventService) {
  }

  ngOnInit() {
    this.eventId = this.route.snapshot.paramMap.get('id');
  }

  show(): void {
    this.positionService.autocomplete(this.eventId).subscribe((positions: PositionAutocompleteModel[]) => {
      this.positions = positions;

      this.modalReference = this.modalService.open(this.modal);
    });
  }

  choose(): void {
    if (!this.selectedPosition) {
      return;
    }

    this.eventService.createPosition(this.eventId, this.selectedPosition.id).subscribe(() => {
      this.modalReference.close();
    });
  }

  createPosition(): void {
    this.modalReference.close();
    this.router.navigate(['/position/event', this.eventId]);
  }

}
