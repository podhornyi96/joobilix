import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {CreatePositionModel} from './position/create-position.model';
import {PositionModel} from './position/position.model';
import {GenericPaginationResult} from '../shared/models/common/generic-pagination-result';
import {PositionSummary} from './positions-table/models/position-summary';
import {PositionAutocompleteModel} from './add-position-modal/position-autocomplete.model';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private http: HttpClient) {
  }

  create(input: CreatePositionModel): Observable<PositionModel> {
    return this.http.post<PositionModel>(`${environment.apiUrl}/api/v2/Positions/Position`, input);
  }

  getAll(skip: number, limit: number, name: string = ''): Observable<GenericPaginationResult<PositionSummary>> {
    return this.http.get<GenericPaginationResult<PositionSummary>>(`${environment.apiUrl}/api/v2/Positions/Summary`, {
      params: {
        skip: skip.toString(),
        limit: limit.toString(),
        name: name
      }
    });
  }

  update(positionId: string, input: CreatePositionModel): Observable<PositionModel> {
    return this.http.put<PositionModel>(`${environment.apiUrl}/api/v2/Positions/${positionId}`, input);
  }

  get(positionId: string): Observable<PositionModel> {
    return this.http.get<PositionModel>(`${environment.apiUrl}/api/v2/Positions/${positionId}`);
  }

  autocomplete(eventId: string): Observable<PositionAutocompleteModel[]> {
    return this.http.get<PositionAutocompleteModel[]>(`${environment.apiUrl}/api/v2/Positions/Autocomplete?eventId=${eventId}`);
  }

  getAllByEvent(skip: number, limit: number, eventId: string): Observable<GenericPaginationResult<PositionSummary>> {
    return this.http.get<GenericPaginationResult<PositionSummary>>(`${environment.apiUrl}/api/v2/Events/${eventId}/Positions/Summary`, {
      params: {
        skip: skip.toString(),
        limit: limit.toString()
      }
    });
  }
}
