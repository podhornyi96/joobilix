import {LocationModel} from '../../events/event/event.model';

export class CreatePositionModel {

  eventId: string;
  title: string;
  field: string;
  industry: string;
  description: string;
  qualifications: string;
  responsibilitis: string;
  location: LocationModel;
  employmentType: number;
  disabilities: boolean;

}
