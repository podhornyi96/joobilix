import {CreatePositionModel} from './create-position.model';

export class PositionModel extends CreatePositionModel {

  id: string;
  open: boolean;

}
