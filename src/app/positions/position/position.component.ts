import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {OrganizationService} from '../../profile/organization.service';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {CreatePositionModel} from './create-position.model';
import {LocationHelper} from '../../shared/helpers/locationHelper';
import {PositionService} from '../position.service';
import {PositionModel} from './position.model';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationModel} from '../../events/event/event.model';
import {Location} from '@angular/common';
import {AddContactModalComponent} from '../../profile/add-contact-modal/add-contact-modal.component';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.less']
})
export class PositionComponent implements OnInit {

  @ViewChild('positionForm') positionForm: NgForm;
  @ViewChild(AddContactModalComponent) addContactModal: AddContactModalComponent;

  @Input() set position(value: PositionModel) {
    this.createPositionModel = value;
    this.positionModel = value;
  }

  positionId: string;

  createPositionModel: CreatePositionModel = new CreatePositionModel();

  string = String;

  submitted = false;

  locationInvalid = false;

  userSettings = {
    showRecentSearch: false,
    showCurrentLocation: false,
    showSearchButton: false,
    inputPlaceholderText: 'location',
    inputString: ''
  };

  employmentTypes: any[] = [
    {id: 0, text: 'Full Time'},
    {id: 1, text: 'Part Time'},
    {id: 2, text: 'Internship'}
  ];

  private positionModel: PositionModel;

  constructor(public organizationService: OrganizationService, private route: ActivatedRoute,
              private router: Router, private positionService: PositionService,
              private location: Location, private spinner: LoaderService) {
  }

  ngOnInit() {
    this.initPosition();
  }


  autoCompleteCallback(response: any) {
    const selectedLocation: LocationModel = LocationHelper.getLocationFromResponse(response);

    if (!selectedLocation) {
      return;
    }

    this.createPositionModel.location = selectedLocation;
    this.checkLocation(this.createPositionModel.location);
  }

  onSubmit(): void {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    const isLocationInvalid = this.checkLocation(this.createPositionModel.location);
    if (!FormHelper.isFormValid(this.positionForm) || isLocationInvalid) {
      this.submitted = false;
      return;
    }

    if (this.positionModel && this.positionModel.id) {
      this.positionService.update(this.positionModel.id, this.createPositionModel)
        .finally(() => this.submitted = false)
        .subscribe((postitionResponse: PositionModel) => {
        this.processSave(postitionResponse);
      });
    } else {
      this.positionService.create(this.createPositionModel)
        .finally(() => this.submitted = false)
        .subscribe((postitionResponse: PositionModel) => {
        this.processSave(postitionResponse);
      });
    }
  }

  private processSave(positionResponse: PositionModel): void {
    this.position = positionResponse;

    //TODO: change request - disable invite modal after add position
    // if (String.isNullOrEmpty(this.positionId)) {
    //   this.addContactModal.show();
    // } else {
    //   this.location.back();
    // }

    this.location.back();
  }

  private initPosition(): void {
    this.spinner.start();
    this.positionId = this.route.snapshot.paramMap.get('positionId');
    const eventId: string = this.route.snapshot.paramMap.get('eventId');

    if (!String.isNullOrEmpty(eventId)) {
      this.createPositionModel.eventId = eventId;
    }

    if (!String.isNullOrEmpty(this.positionId)) {
      this.positionService.get(this.positionId)
        .finally(() => this.spinner.complete())
        .subscribe((position: PositionModel) => {
        this.position = position;
        this.updatePlaceName(position.location.placeName);
      });
    } else {
      this.organizationService.getProfile()
        .finally(() => this.spinner.complete())
        .subscribe((resp) => {
        this.createPositionModel.location = resp.location;
        this.updatePlaceName(resp.location.placeName);
      });
    }
  }

  private updatePlaceName(placeName: string) {
    this.userSettings.inputString = placeName;
    // according to documentation to update location
    this.userSettings = Object.assign({}, this.userSettings);
  }

  // required, longitude, latitude, country, city
  // return true if invalid
  private checkLocation(location: LocationModel): boolean {
    this.locationInvalid = !(location
      && location.city
      && location.country
      && location.latitude
      && location.longitude);

    return this.locationInvalid;
  }

}
