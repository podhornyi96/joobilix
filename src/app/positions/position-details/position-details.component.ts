import {Component, Input, OnInit} from '@angular/core';
import {PositionModel} from '../position/position.model';

@Component({
  selector: 'jblx-position-details',
  templateUrl: './position-details.component.html',
  styleUrls: ['./position-details.component.less']
})
export class PositionDetailsComponent implements OnInit {

  @Input() positionDetails: PositionModel;

  constructor() { }

  ngOnInit() {
  }

}
