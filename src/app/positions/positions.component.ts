import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PositionService} from './position.service';
import {Observable, Subject} from 'rxjs/Rx';
import {PositionSummary} from './positions-table/models/position-summary';
import {PositionsTableData} from './positions-table/models/positions-table-data';
import {PositionModel} from './position/position.model';
import {Router} from '@angular/router';
import {SideModalComponent} from '../shared/modals/side-modal/side-modal.component';
import {GenericPaginationResult} from '../shared/models/common/generic-pagination-result';
import {PositionsTableComponent} from './positions-table/positions-table.component';
import {AddPositionModalComponent} from './add-position-modal/add-position-modal.component';
import {EventService} from '../events/event.service';
import {LoaderService} from '../shared/loader/loader.service';

@Component({
  selector: 'jblx-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.less']
})
export class PositionsComponent implements OnInit, OnDestroy {

  @Input() eventId: string;

  @ViewChild(PositionsTableComponent) positionsTable: PositionsTableComponent;
  @ViewChild('sideModal') sideModal: SideModalComponent;
  @ViewChild(AddPositionModalComponent) addPositionModal: AddPositionModalComponent;

  positions: PositionsTableData;
  selectedPosition: PositionModel;
  private unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private stepLoad = 100;

  constructor(private router: Router, private _positionService: PositionService, private eventService: EventService,
              private spinner: LoaderService) {
  }

  ngOnInit() {
    this.spinner.start();

    this.getAllPositions();
  }

  getAllPositions(skip: number = 0, limit: number = 50, prevRequest: PositionSummary[] = []) {
    if (skip === 0) {
      this.positions = undefined;
    }

    this.getAllPositionsServiceToggle(skip, limit)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data.length) {
          this.spinner.complete();
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepLoad;
          this.getAllPositions(skip, limit, prevRequest);
        } else {
          response.data.sort((left, right) => {
            return ('' + left.title).localeCompare(right.title);
          });
          this.initPositionsTableData(response.data);
        }
      });
  }

  getAllPositionsServiceToggle(skip: number, limit: number): Observable<GenericPaginationResult<PositionSummary>> {
    if (this.eventId) {
      return this._positionService.getAllByEvent(skip, limit, this.eventId);
    }

    return this._positionService.getAll(skip, limit);
  }

  initPositionsTableData(positions: PositionSummary[]) {
    this.spinner.complete();

    this.positions = new PositionsTableData();
    this.positions.onClickRowHandler = (position) => {
      this.showPositionDetails(position.id);
    };
    this.positions.positions = positions;
    this.positions.actions = [
      {
        title: 'Edit',
        action: (data: PositionSummary) => {
          this.navigateToPosition(data);
        }
      },
      {
        title: 'Duplicate',
        action: (data: PositionSummary) => {
          this.duplicatePosition(data.id);
        }
      },
      {
        title: 'Hired',
        action: (data: PositionSummary) => {
          console.log('hired');
        }
      }
    ];

    if (this.eventId) {
      this.positions.actions.push({
        title: 'Delete',
        action: (data: PositionSummary) => {
          this.deletePosition(data.id);
        }
      });
    }
  }

  private duplicatePosition(positionId: string): void {
    this._positionService.get(positionId).subscribe((position: PositionModel) => {

      position.eventId = this.eventId;
      position.id = null;
      position.title = 'copy of ' + position.title;

      this._positionService.create(position).subscribe((createdPosition: PositionModel) => {
        this.router.navigate(['/position', createdPosition.id]);
      });

    });
  }

  addPosition(): void {
    if (!this.eventId) {
      this.router.navigate(['/position']);
      return;
    }

    this.addPositionModal.show();
  }

  private deletePosition(positionId: string): void {
    this.eventService.deleteEventPosition(this.eventId, positionId).subscribe(() => {

      this.positionsTable.removeRow(positionId);
    });
  }

  private navigateToPosition(data: PositionSummary): void {
    if (this.eventId) {
      this.router.navigate([`/position/${data.id}/event/${this.eventId}`]);
    } else {
      this.router.navigate(['/position', data.id]);
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  showPositionDetails(selectedPositionId: string): void {
    console.log(selectedPositionId);
    if (!selectedPositionId) {
      return;
    }

    this._positionService.get(selectedPositionId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.selectedPosition = response;
        this.sideModal.show('lg');
      });
  }
}
