import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AccountService} from './account/account.service';
import {RedirectHelper} from './shared/helpers/redirectHelper';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private accountService: AccountService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.accountService.isLoggedIn()) {

      this.router.navigate(['/home', {openLogin: true}]);

      return false;
    }

    return this.accountService.getCurrentProfile().toPromise().then((currentProfile) => {
      const nextUrl = this.getUrlFromRouter(next);
      if (!this.accountService.isLoginStepsDone(nextUrl)) {
        const redirectUrl = RedirectHelper.getLoginFlowRedirectUrl(currentProfile);

        this.router.navigate([redirectUrl]);

        return Promise.resolve(false);
      }

      return Promise.resolve(true);
    });
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  private getUrlFromRouter(router: ActivatedRouteSnapshot): string {
    const result = router.pathFromRoot
      .filter(p => p.routeConfig && p.routeConfig.path)
      .map(p => p.routeConfig.path).join('/');
    return result;
  }
}
