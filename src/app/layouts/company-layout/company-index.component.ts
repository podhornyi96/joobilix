import 'rxjs/add/operator/takeUntil';

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import {EventStatusEnum, HostEvent} from '../../shared/models/host/event/host-event.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventItemMenuModel} from '../host-layout/view-model/event-item-menu.model';
import {HostService} from '../../host/host.service';
import {EventInteractionService} from '../../shared/services/interactions/event-interaction.service';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-company-index',
  templateUrl: './company-index.component.html',
  styleUrls: ['./company-index.component.less']
})
export class CompanyIndexComponent implements OnInit, OnDestroy {

  eventMenuItems: EventItemMenuModel[];
  eventStatus = EventStatusEnum;
  isEventMenuCollapsed = true;
  isProfileMenuCollapsed = false;
  ignoreNewLabelEventIds: string[];

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private stepHostEventLoad = 50;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private spinner: LoaderService,
    private _hostApiService: HostService,
    private _eventInteraction: EventInteractionService) {

    this.eventMenuItems = [];
    this.ignoreNewLabelEventIds = [];
    _activatedRoute.url.subscribe(() => this.setCollapsEventsMenu());
    _eventInteraction.chooseNewInvitation$.subscribe((eventId) => this.ignoreNewLabelEventIds.push(eventId));
  }

  ngOnInit() {
    this.spinner.start();

    this.getAllEvents();
  }

  getAllEvents(skip: number = 0, limit: number = 50, prevRequest: HostEvent[] = []) {
    if (skip === 0) {
      this.eventMenuItems = [];
    }

    this._hostApiService.getHostEvents(skip, limit)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data.length) {
          this.spinner.complete();
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepHostEventLoad;
          this.getAllEvents(skip, limit, prevRequest);
        } else {
          let responseEventMenu = prevRequest.filter(d => d.eventStatus !== this.eventStatus.deleted)
            .map(d => EventItemMenuModel.fromHostEventModel(d));
          this.eventMenuItems = this.eventMenuItems.concat(responseEventMenu);

          this.spinner.complete();
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  routeToggleNewEvent(id: number, eventStatus: EventStatusEnum) {
    this._router.navigateByUrl(`/company/event/${id}`);
  }

  private setCollapsEventsMenu() {
    this.isEventMenuCollapsed = this._activatedRoute.snapshot.firstChild.url[0].path !== 'event'
      && this._activatedRoute.snapshot.firstChild.url[0].path !== 'events';
  }
}
