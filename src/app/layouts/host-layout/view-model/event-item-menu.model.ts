import {EventStatusEnum, HostEvent} from '../../../shared/models/host/event/host-event.model';
import * as moment from 'moment';

export class EventItemMenuModel {
  constructor(
    public id: string,
    public eventStatus: EventStatusEnum,
    public title: string,
    public when: moment.Moment) {}

  static fromHostEventModel(data: HostEvent): EventItemMenuModel {
    let result: EventItemMenuModel = new EventItemMenuModel(data.id, data.eventStatus, data.title, data.when);
    return result;
  }
}
