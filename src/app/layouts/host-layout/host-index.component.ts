import 'rxjs/add/operator/takeUntil';

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import {EventItemMenuModel} from './view-model/event-item-menu.model';
import {EventStatusEnum, HostEvent} from '../../shared/models/host/event/host-event.model';
import {ActivatedRoute} from '@angular/router';
import {HostService} from '../../host/host.service';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-host-index',
  templateUrl: './host-index.component.html',
  styleUrls: ['./host-index.component.less']
})
export class HostIndexComponent implements OnInit, OnDestroy {

  eventMenuItems: EventItemMenuModel[];
  eventStatus = EventStatusEnum;
  isEventMenuCollapsed = false;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private stepHostEventLoad = 50;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private spinner: LoaderService, private _hostApiService: HostService) {

    this.eventMenuItems = [];
    _activatedRoute.url.subscribe(() => this.setCollapsEventsMenu());
  }

  ngOnInit() {
    this.spinner.start();

    this.getAllEvents();
  }

  getAllEvents(skip: number = 0, limit: number = 50, prevRequest: HostEvent[] = []) {
    if (skip === 0) {
      this.eventMenuItems = [];
    }

    this._hostApiService.getHostEvents(skip, limit)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data.length) {
          this.spinner.complete();
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepHostEventLoad;
          this.getAllEvents(skip, limit, prevRequest);
        } else {
          let responseEventMenu = prevRequest.filter(d => d.eventStatus !== this.eventStatus.deleted)
            .map(d => EventItemMenuModel.fromHostEventModel(d));
          this.eventMenuItems = this.eventMenuItems.concat(responseEventMenu);

          this.spinner.complete();
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  private setCollapsEventsMenu() {
    this.isEventMenuCollapsed = this._activatedRoute.snapshot.firstChild.url[0].path !== 'event'
      && this._activatedRoute.snapshot.firstChild.url[0].path !== 'events';
  }
}
