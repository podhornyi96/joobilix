import {Component} from '@angular/core';

import './shared/helpers/stringHelper';

@Component({
  selector: 'app-root',
  template: '<jblx-site-layout></jblx-site-layout>'
})
export class AppComponent {

}
