import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jblx-side-modal',
  templateUrl: './side-modal.component.html',
  styleUrls: ['./side-modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class SideModalComponent implements OnInit {

  @ViewChild('content') modal: ElementRef;

  constructor(private modalService: NgbModal) {

  }

  ngOnInit() {
  }

  show(size?: 'sm' | 'lg') {
    const options: NgbModalOptions = {
      windowClass: 'side-modal',
      size: size
    };

    this.modalService.open(this.modal, options);
  }
}
