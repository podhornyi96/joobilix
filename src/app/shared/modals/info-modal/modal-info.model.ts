export class ModalInfoModel {
  title: string;
  message: string;
  closeBtnTxt: string;
  actionBtnTxt: string;
  isCloseBtn: boolean;
  isActionBtn: boolean;
  classCloseBtn: string;
  classActionBtn: string;
  actionCallBackFunc: () => void;
}
