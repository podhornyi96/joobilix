import {ModalInfoModel} from './modal-info.model';

export class ModalInfoSettingsDefaultConstants {
  static get simpleInfo(): ModalInfoModel {
    let result = new ModalInfoModel();
    result.isCloseBtn = true;
    result.classCloseBtn = 'btn-yellow';
    result.closeBtnTxt = 'Close';
    return result;
  }

  static get confirmation(): ModalInfoModel {
    let result = new ModalInfoModel();
    result.isCloseBtn = true;
    result.classCloseBtn = 'btn-default';
    result.closeBtnTxt = 'Cancel';
    result.isActionBtn = true;
    result.classActionBtn = 'btn-yellow';
    result.actionBtnTxt = 'Ok';
    return result;
  }
}
