import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HostService } from '../../../host/host.service';
import { ModalInfoModel } from './modal-info.model';

@Component({
  selector: 'jblx-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.less']
})
export class InfoModalComponent {

  @Input() model: ModalInfoModel;

  constructor(public activeModal: NgbActiveModal) {}

  onActionBtnClick() {
    this.model.actionCallBackFunc();
  }
}
