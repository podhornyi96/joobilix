import {Component, OnInit, ElementRef, ViewChild, ViewEncapsulation, HostListener} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {AccountService} from '../../../account/account.service';
import {CurrentProfile} from '../../../account/current-profile.model';
import {RedirectHelper} from '../../helpers/redirectHelper';

@Component({
  selector: 'terms-modal',
  templateUrl: './terms-modal.component.html',
  styleUrls: ['./terms-modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class TermsModalComponent implements OnInit {

  @ViewChild('content') modal: ElementRef;

  termsRead = false;

  constructor(private modalService: NgbModal, private router: Router, private accountService: AccountService) {
  }

  ngOnInit() {
  }

  openModal(): void {
    this.modalService.open(this.modal, {size: 'lg', backdrop: 'static'});
  }

  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    const pos = event.target.offsetHeight + event.target.scrollTop;
    const max = event.target.scrollHeight;

    if (Math.ceil(pos) === max && !this.termsRead) {
      this.termsRead = true;
    }
  }

  accept(): void {
    this.accountService.agreeTerms().subscribe(() => {
      this.accountService.getCurrentProfile(true).subscribe((profile: CurrentProfile) => {
        const redirectUrl = RedirectHelper.getLoginFlowRedirectUrl(profile);

        this.router.navigate([redirectUrl]);
      });
    });
  }

}
