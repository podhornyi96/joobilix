import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';

import {AccountService} from '../../account/account.service';
import {RefreshTokenRequest} from '../../account/login/refresh-token.model';
import {LoginResponse} from '../../account/login/login-response.model';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  private IsRefreshInProgress: boolean;
  tokenSubject: BehaviorSubject<LoginResponse> = new BehaviorSubject<LoginResponse>(null);

  constructor(private accountService: AccountService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.accountService.isNeedRefreshToken() || req.url.includes('api/v2/Accounts/Token')) {
      return next.handle(req);
    }

    return this.refreshToken(req, next);
  }

  refreshToken(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.IsRefreshInProgress) {
      this.IsRefreshInProgress = true;
      this.tokenSubject.next(null);

      const loginData = this.accountService.getLoginData();
      return this.accountService.refreshToken(new RefreshTokenRequest({
        refreshToken: loginData.refreshToken,
        sessionToken: loginData.accessToken
      }))
        .switchMap(
          (newToken: LoginResponse) => {
            if (newToken) {
              this.tokenSubject.next(newToken);
              this.accountService.setSession(newToken);
              return next.handle(req);
            } else {
              this.accountService.logoutUser(false).subscribe(() => location.reload(true));
            }
          })
        .catch(err => {
          // const error = err.error.message || err.statusText;
          return throwError(err);
        })
        .finally(() => {
          this.IsRefreshInProgress = false;
        });
    } else {
      return this.tokenSubject
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(req);
        });
    }
  }
}
