import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {AccountService} from '../../account/account.service';

@Injectable()
export class UnauthErrorInterceptor implements HttpInterceptor {
  constructor(private accountService: AccountService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(err => {
      if (err.status === 401) {
        this.accountService.logoutUser(false).subscribe(() => {
          location.reload(true);
        });
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
