import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {LoginResponse} from '../../account/login/login-response.model';
import {AccountService} from '../../account/account.service';
import {LocalStorageKeysConstant} from '../constants/local-storage-keys.constant';

@Injectable()
export class ApplyTokenInterceptor implements HttpInterceptor {

  private isRefreshingToken = false;
  private tokenSubject: BehaviorSubject<LoginResponse> = new BehaviorSubject<LoginResponse>(null);

  constructor(private accountService: AccountService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const loginData = this.accountService.getLoginData();

    if (!loginData) {
      return next.handle(req);
    }

    const authRequest = this.addToken(req);
    return next.handle(authRequest);
  }

  private addToken(req: HttpRequest<any>): HttpRequest<any> {
    const currentUserInfo: LoginResponse = JSON.parse(localStorage.getItem(LocalStorageKeysConstant.CurrentUser));

    if (!currentUserInfo || !currentUserInfo.accessToken) {
      return req;
    }

    return req.clone({
      headers: req.headers.set('Authorization', `Bearer ${currentUserInfo.accessToken}`)
    });
  }
}
