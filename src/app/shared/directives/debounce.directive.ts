import {Directive, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {NgControl} from '@angular/forms';
import {Subject} from 'rxjs';

@Directive({
  selector: '[ngModel][debounce]',
})
export class DebounceDirective implements OnInit, OnDestroy {

  @Output() debounce = new EventEmitter<any>();

  @Input('debounceTime') debounceTime = 500;

  private isFirstChange = true;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(public model: NgControl) {
  }

  ngOnInit() {
    this.model.valueChanges
      .takeUntil(this.ngUnsubscribe)
      .debounceTime(this.debounceTime)
      .distinctUntilChanged()
      .subscribe(modelValue => {
        if (this.isFirstChange) {
          this.isFirstChange = false;
        } else {
          this.debounce.emit(modelValue);
        }
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
