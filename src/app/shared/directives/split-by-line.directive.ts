import {AfterViewInit, Component, Directive, ElementRef, HostBinding, Input, OnInit} from '@angular/core';
import {EllipsisPipe} from '../pipes/ellipsis.pipe';

@Directive({
  selector: '[jblxSplitByLine]',
})
export class SplitByLineDirective implements AfterViewInit {

  @Input() maxline: number;
  @Input() maxCharacters: number;
  private template: string;

  constructor(private elementRef: ElementRef, private ellipsisPipe: EllipsisPipe) {}



  ngAfterViewInit() {
    this.template = this.elementRef.nativeElement.innerText;
    this.splitText();
  }

  private splitText() {
    if (!this.template || this.template.length <= this.maxCharacters) {
      return;
    }

    let numberLine = 0;
    const lines: string[] = [];
    do {
      numberLine++;
      let newLine = this.template.substring(0, this.maxCharacters);
      this.template = this.template.substring(this.maxCharacters);
      const isNeedHyphen = newLine.slice(-1).match(/[a-zA-Z]/) && this.template.slice(0, 1).match(/[a-zA-Z]/);
      if (isNeedHyphen) {
        newLine += '-';
      }
      lines.push(newLine);
    } while (numberLine < this.maxline - 1 && this.template.length > this.maxCharacters);

    this.template = this.ellipsisPipe.transform(this.template, this.maxCharacters);

    lines.push(this.template);

    this.template = lines.join('<br>');
    this.elementRef.nativeElement.innerHTML = this.template;
  }
}
