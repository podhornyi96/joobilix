import {Directive, HostBinding, Input} from '@angular/core';
import {Moment} from 'moment';

import * as moment from 'moment';

@Directive({
  selector: '[jblx-expired-date]'
})
export class ExpiredDateDirective {

  constructor() { }

  @Input() time: Moment;
  @Input() expiredClass: string;

  @HostBinding('class') get currentClass() {
    let isExpired = false;
    if (this.time) {
      isExpired = moment.now().valueOf() > this.time.valueOf();
    }

    return isExpired ? this.expiredClass : '';
  }
}
