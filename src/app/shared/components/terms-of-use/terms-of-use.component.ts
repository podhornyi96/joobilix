import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'jblx-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class TermsOfUseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
