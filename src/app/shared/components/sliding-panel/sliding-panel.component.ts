import {Component, EventEmitter, OnInit, Output, Renderer2, ViewEncapsulation} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'jblx-sliding-panel',
  templateUrl: './sliding-panel.component.html',
  styleUrls: ['./sliding-panel.component.less'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('600ms ease-in-out')),
      transition('out => in', animate('600ms ease-in-out'))
    ]),
    trigger('fadeEffectInOut', [
      state('in', style({
        backgroundColor: '#000',
        opacity: 0.7,
        zIndex: 1040,
        position: 'fixed'
      })),
      state('out', style({
        backgroundColor: 'transparent',
        opacity: 0
      })),
      transition('in => out', [
        animate('600ms linear')
      ]),
      transition('out => in', [
        animate('600ms linear')
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class SlidingPanelComponent implements OnInit {

  @Output() opened: EventEmitter<void> = new EventEmitter<void>();
  @Output() closed: EventEmitter<void> = new EventEmitter<void>();

  menuState = 'out';

  constructor(private renderer: Renderer2) {
  }

  ngOnInit() {
  }

  open(): void {
    this.renderer.addClass(document.body, 'modal-load');
    this.menuState = 'in';
    this.opened.emit();
  }

  close(): void {
    this.renderer.removeClass(document.body, 'modal-load');
    this.menuState = 'out';
    this.closed.emit();
  }

}
