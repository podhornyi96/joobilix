import { Pipe, PipeTransform } from '@angular/core';
import {SizeCompanyEnum} from '../../models/host/company/host-company-response.model';

@Pipe({
  name: 'companySize'
})
export class CompanySizePipe implements PipeTransform {

  transform(sizeCompany: SizeCompanyEnum) {
    switch (sizeCompany) {
      case SizeCompanyEnum._0:
        return '1-10';
      case SizeCompanyEnum._1:
        return '10-50';
      case SizeCompanyEnum._2:
        return '50-100';
      case SizeCompanyEnum._3:
        return '100-500';
      case SizeCompanyEnum._4:
        return '500-1000';
      case SizeCompanyEnum._5:
        return '1000+';
    }
  }
}
