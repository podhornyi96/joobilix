import { Pipe, PipeTransform } from '@angular/core';
import {RegistrationStatusEnum} from '../../models/host/company/host-event-company.model';

@Pipe({
  name: 'companyStatusRegister'
})
export class CompanyStatusRegisterPipe implements PipeTransform {

  transform(value: RegistrationStatusEnum): string {
    switch (value) {
      case RegistrationStatusEnum.new:
        return 'Invitation pending';
      case RegistrationStatusEnum.failedToSendlvite:
        return 'Invitation failed';
      case RegistrationStatusEnum.inviteSent:
        return 'Invitation sent';
      case RegistrationStatusEnum.registrationCompleted :
        return 'Registered';
      case RegistrationStatusEnum.deleted:
        return 'Deleted';
      case RegistrationStatusEnum.disagree:
        return 'Declined';
      case RegistrationStatusEnum.agree:
        return 'Registered';
      default:
        return '';
    }
  }

}
