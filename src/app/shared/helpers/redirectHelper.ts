import {CurrentProfile} from '../../account/current-profile.model';
import {RoleConstants} from '../constants/role.constants';
import {PathConstants} from './path.constants';

export class RedirectHelper {

  public static getLoginFlowRedirectUrl(profile: CurrentProfile): string {

    if (profile.needToSetProfile || profile.needToAgreeTerms) {
      return PathConstants.setProfileAndAgreeTerms;
    } else if (profile.needToSetOrganizationProfile) {
      return PathConstants.setOrganizationProfile;
    }

    if (profile.roles.indexOf(RoleConstants.Host) !== -1) {
      return '/host/profile';
    }

    return '/company/about';
  }

}
