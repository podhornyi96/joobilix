import * as moment from 'moment';

export class DateHelper {

  public static isValidHoursAndMinutes(value: string): boolean {
    if (!value) {
      return false;
    }

    const hoursAndMinutes: string[] = value.split(':');

    if (hoursAndMinutes.length !== 2 || isNaN(parseInt(hoursAndMinutes[0])) || isNaN(parseInt(hoursAndMinutes[1])) || value.indexOf('_') !== -1) {
      return false;
    }

    if(parseInt(hoursAndMinutes[0]) > 23 || parseInt(hoursAndMinutes[1]) > 59)
      return false;

    return true;
  }

  public static setMinutesAndHours(targetDate: any, value: string): string {
    const date = moment(targetDate);

    if (!this.isValidHoursAndMinutes(value)) {
      return date.format('MM/DD/YYYY HH:mm');
    }

    const hoursAndMinutes: string[] = value.split(':');

    date.set('hour', +hoursAndMinutes[0]);
    date.set('minute', +hoursAndMinutes[1]);

    return date.format('MM/DD/YYYY HH:mm');
  }

}
