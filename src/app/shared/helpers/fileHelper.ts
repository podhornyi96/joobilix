export class FileHelper {

  public static async convertToDataURLviaCanvas(url, outputFormat): Promise<any> {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = () => {
        let canvas = <HTMLCanvasElement> document.createElement('CANVAS');
        const ctx = canvas.getContext('2d');

        canvas.height = img.height;
        canvas.width = img.width;

        ctx.drawImage(img, 0, 0);

        const dataURL = canvas.toDataURL(outputFormat).split(',').pop();

        resolve(dataURL);
        canvas = null;
      };
      img.src = url;
    });

  }


}
