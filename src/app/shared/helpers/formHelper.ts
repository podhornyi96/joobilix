import {NgForm} from '@angular/forms';

export class FormHelper {
  /**
   * mark all form controlls as dirty
   * and validate form
   * @param form
   */
  public static markControllsAsDirty(form: NgForm): void {
    Object.keys(form.controls).forEach(key => {
      form.controls[key].markAsDirty();
      form.controls[key].markAsTouched();
      form.controls[key].updateValueAndValidity();
    });
  }

  public static isFormValid(form: NgForm, isIgnoreRequired?: boolean): boolean {
    this.markControllsAsDirty(form);
    //  form.updateValueAndValidity();

    if(!form.valid && isIgnoreRequired) {
      let result = this.isAllErrorsRequired(form);
      return result;
    }

    return form.valid;
  }

  private static isAllErrorsRequired(form: NgForm): boolean {
    let result = true;

    Object.keys(form.controls).forEach(key => {
      if (form.controls[key].errors) {
        result = result && (form.controls[key].errors.required || !form.controls[key].value);
      }
    });

    return result;
  }
}
