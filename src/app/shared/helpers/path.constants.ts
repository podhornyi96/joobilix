export class PathConstants {
  public static setOrganizationProfile = '/profile/company';
  public static setProfileAndAgreeTerms = '/personal-info';
}
