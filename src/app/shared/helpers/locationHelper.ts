import {LocationModel} from '../../events/event/event.model';

export class LocationHelper {

  public static getLocationFromResponse(response: any): LocationModel {
    if (!response) {
      return null;
    }

    const location: LocationModel = new LocationModel();

    location.latitude = response.data.geometry.location.lat;
    location.longitude = response.data.geometry.location.lng;

    location.placeName = response.data.formatted_address;

    for (const addressComponent of response.data.address_components) {
      if (addressComponent.types.indexOf('country') !== -1) {
        location.country = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('locality') !== -1) {
        location.city = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('administrative_area_level_1') !== -1) {
        location.state = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('route') !== -1) {
        location.street = addressComponent.long_name;
      }

      if (addressComponent.types.indexOf('street_number') !== -1) {
        location.streetNumber = addressComponent.long_name;
      }
    }

    return location;
  }

}
