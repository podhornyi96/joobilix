
export class ObjectHelper {

  public static areObjectEquals(target: any, source: any): boolean {
    return JSON.stringify(target) === JSON.stringify(source);
  }

}
