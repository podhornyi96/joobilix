interface StringConstructor {
  isNullOrEmpty(str: string): boolean;
  formatDateMMDDYYYY(value: string): string;
  formatDateDDMMMYYYY(value: string): string;
  getEnumValue(value: string, numLink: any): string;
  isUrl(value: string): boolean;
}

String.isNullOrEmpty = (value: string) => {
  if(value != null && typeof value == "number")
    return false;

  return (value == null || (value == "") || value.length == 0);
};

String.formatDateMMDDYYYY = (value: string) => {
  let date = new Date(value);

  return (date.getMonth() + 1) +
    "-" + date.getDate() +
    "-" + date.getFullYear();
};

String.formatDateDDMMMYYYY = (value:string) => {
  let date = new Date(value);
  let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

  return date.getDate() +
    "-" + months[date.getMonth()] +
    "-" + date.getFullYear();
};

String.getEnumValue = function (value: string, enumLink: any): string {
  let result = enumLink[value];

  if (result && typeof result != 'undefined')
    return result.toString();
  else
    return value;
};

String.isUrl = function isURL(str: string) : boolean {
  const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

  return pattern.test(str);
};
