import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../../account/account.service';
import {ProfileInfo} from '../../models/host/profile/profile-info.model';
import {AccountInteractionService} from '../../../account/account-interaction.service';
import {Router} from '@angular/router';
import {CurrentProfile} from '../../../account/current-profile.model';
import {RoleConstants} from '../../constants/role.constants';

@Component({
  selector: 'jblx-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.less']
})
export class HeaderBarComponent implements OnInit {

  profile: ProfileInfo;
  isUserLogged: boolean;

  constructor(
    private _accountInteractionService: AccountInteractionService,
    private accountService: AccountService, private router: Router) {

    _accountInteractionService.refreshedProfileInfo$
      .subscribe((result) => {
        this.updateViewProfile(result);
      });

    this.initDefaultProfile();
  }

  ngOnInit() {
    this.isUserLogged = this.accountService.isLoggedIn();
    if (this.isUserLogged) {
      this.initAccount();
    }
  }

  initAccount() {
    this.accountService.getUserAccount()
      .subscribe((response) => {
        this.updateViewProfile(response);
      });
  }

  initDefaultProfile() {
    this.profile = new ProfileInfo('User', 'Name', '../../../../assets/images/user-image-default.png');
  }

  private updateViewProfile(profileInfo: ProfileInfo) {
    this.profile = profileInfo;
    this.isUserLogged = this.accountService.isLoggedIn();
  }

  signOut(): void {
    this.accountService.logoutUser();
  }

  editProfile(): void {
    this.accountService.getCurrentProfile().subscribe((profile: CurrentProfile) => {
      if (profile.roles.indexOf(RoleConstants.Host) !== -1) {
        this.router.navigate(['/host/profile/edit']);
      } else {
        this.router.navigate(['/profile/company']);
      }
      // TODO: here can be more roles. Handle this
    });
  }

}
