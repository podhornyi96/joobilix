import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jblx-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  tel = '(972)-524557114';
  email = 'hello@joobilix.com';
  termsOfUseLabel = 'Terms of use';
  privacyPolicyLabel = 'Privacy policy';
  aboutLabel = 'About';

  constructor() { }

  ngOnInit() {
  }

}
