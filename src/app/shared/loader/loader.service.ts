import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Rx';

@Injectable()
export class LoaderService {
  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();

  constructor() { }

  start() {
    this.loaderSubject.next(<LoaderState>{show: true});
  }

  complete() {
    this.loaderSubject.next(<LoaderState>{show: false});
  }
}

export interface LoaderState {
  show: boolean;
}
