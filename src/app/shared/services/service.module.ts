import {NgModule} from '@angular/core';
import {AccountService} from '../../account/account.service';
import {AccountInteractionService} from '../../account/account-interaction.service';
import {WindowRef} from './window-ref';
import {CoreService} from './core.service';

@NgModule({
  providers: [
    AccountService,
    AccountInteractionService,
    WindowRef,
    CoreService]
})
export class ServiceModule {
}
