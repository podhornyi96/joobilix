import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class EventInteractionService {

  private chooseNewInvitationSource = new Subject<string>();
  chooseNewInvitation$ = this.chooseNewInvitationSource.asObservable();

  chooseNewInvitation(eventId: string) {
    this.chooseNewInvitationSource.next(eventId);
  }
}
