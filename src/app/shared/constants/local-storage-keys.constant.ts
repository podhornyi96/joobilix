export class LocalStorageKeysConstant {
  public static get UserProfileInfo(): string {
    return 'userProfileInfo';
  }

  public static get CurrentUser(): string {
    return 'currentUser';
  }

}
