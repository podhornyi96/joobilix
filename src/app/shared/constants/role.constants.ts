export class RoleConstants {

  public static get HR() {
    return 'HR';
  }

  public static get Recruiter() {
    return 'Recruiter';
  }

  public static get Host() {
    return 'Host';
  }

  public static get RecruiterManager() {
    return 'RecruiterManager';
  }

}
