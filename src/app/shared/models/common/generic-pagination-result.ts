export class GenericPaginationResult<T> {
  count: number;
  total: number;
  data: T[];
}
