export class Page {
  pageNumber: number;
  size: number;
  totalElements: number;

  private skip: number;

  constructor(
    totalElements: number,
    size: number,
    skip: number
  ) {
    this.totalElements = totalElements;
    this.size = size;
    this.skip = skip;

    this.initPageNumber();
  }

  initPageNumber() {
    this.pageNumber = Math.floor(this.skip / this.size);
  }
}
