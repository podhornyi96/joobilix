export class ProfileInfo {
  constructor(public firstName: string, public lastName: string, public logoUrl: string) {}

  init(data?: any) {
    if (data) {
      this.firstName = data['firstName'];
      this.lastName = data['lastName'];
      this.logoUrl = data['logoUrl'];
    }
  }

  static fromJS(data: any): ProfileInfo {
    let result = new ProfileInfo(data.firstName, data.lastName, data.logoUrl);
    return result;
  }
}
