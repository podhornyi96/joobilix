export class HostProfileDataResponse {
  instituteName: string;
  firstName: string;
  lastName: string;
  aboutInstitute: string;
  logoUrl: string;
  emailAddress: string;
  countryPhoneNumberPrefix: string;
  phoneNumber: string;

  init(data?: any) {
    if (data) {
      this.instituteName = data['instituteName'];
      this.firstName = data['firstName'];
      this.lastName = data['lastName'];
      this.aboutInstitute = data['aboutInstitute'];
      this.logoUrl = data['logoUrl'];
      this.emailAddress = data['emailAddress'];
      this.countryPhoneNumberPrefix = data['countryPhoneNumberPrefix'];
      this.phoneNumber = data['phoneNumber'];
    }
  }

  static fromJS(data: any): HostProfileDataResponse {
    let result = new HostProfileDataResponse();
    result.init(data);
    return result;
  }
}
