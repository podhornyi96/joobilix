import {HostProfileDataResponse} from './host-profile-data-response.model';

export class UpdateProfileRequest {
  instituteName: string;
  firstName: string;
  lastName: string;
  aboutInstitute: string;
  imageData: string;
  countryPhoneNumberPrefix: string;
  phoneNumber: string;

  init(data?: any) {
    if (data) {
      this.instituteName = data['instituteName'];
      this.firstName = data['firstName'];
      this.lastName = data['lastName'];
      this.aboutInstitute = data['aboutInstitute'];
      this.imageData = data['imageData'];
      this.countryPhoneNumberPrefix = data['countryPhoneNumberPrefix'];
      this.phoneNumber = data['phoneNumber'];
    }
  }

  static fromJS(data: any): UpdateProfileRequest {
    let result = new UpdateProfileRequest();
    result.init(data);
    return result;
  }

  static fromHostProfileDataResponse(data: HostProfileDataResponse) {
    let result = UpdateProfileRequest.fromJS(data);
    return result;
  }
}
