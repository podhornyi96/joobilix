export class HostAddCompanyToEventRequestModel {
  companyName: string;
  firstName: string;
  lastName: string;
  emailAddress: string;

  constructor() {
    this.companyName = '';
    this.firstName = '';
    this.lastName = '';
    this.emailAddress = '';
  }
}
