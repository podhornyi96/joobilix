import {IBaseModel} from '../../base.model';
import {HostEventCompanyModel} from './host-event-company.model';

export class HostEventCompanyPageResponseModel implements IBaseModel, IHostEventCompanyPageResponseModel {
  count: number;
  total: number;
  data: HostEventCompanyModel[];

  init(data?: any) {
    if (data) {
      this.count = data['count'];
      this.total = data['total'];
      if (data['data'] && data['data'].constructor === Array) {
        this.data = [];
        for (let item of data['data'])
          this.data.push(HostEventCompanyModel.fromJS(item));
      }
    }
  }

  static fromJS(data: any): HostEventCompanyPageResponseModel {
    let result = new HostEventCompanyPageResponseModel();
    result.init(data);
    return result;
  }
}

export interface IHostEventCompanyPageResponseModel {
  count: number;
  total: number;
  data: HostEventCompanyModel[];
}
