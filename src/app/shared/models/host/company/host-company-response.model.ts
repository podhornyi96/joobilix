import {HostCompanyContactDetailsModel} from './host-company-contact-details.model';
import {IBaseModel} from '../../base.model';

export class HostCompanyResponseModel implements IBaseModel, IHostCompanyResponseModel {
  id: string;
  name: string;
  logoUrl: string;
  oneLiner: string;
  industry1: string;
  industry2: string;
  size: SizeCompanyEnum;
  description: string;
  positions: string[];
  contactDetailses: HostCompanyContactDetailsModel[];

  init(data?: any) {
    if (data) {
      this.id = data['id'];
      this.name = data['name'];
      this.logoUrl = data['logoUrl'];
      this.oneLiner = data['oneLiner'];
      this.industry1 = data['industry1'];
      this.industry2 = data['industry2'];
      this.size = data['size'];
      this.description = data['description'];
      this.positions = data['positions'];

      if (data['contactDetailses'] && data['contactDetailses'].constructor === Array) {
        this.contactDetailses = [];
        for (let item of data['contactDetailses'])
          this.contactDetailses.push(HostCompanyContactDetailsModel.fromJS(item));
      }
    }
  }

  static fromJS(data: any): HostCompanyResponseModel {
    let result = new HostCompanyResponseModel();
    result.init(data);
    return result;
  }
}

export interface IHostCompanyResponseModel {
  id: string;
  name: string;
  logoUrl: string;
  oneLiner: string;
  industry1: string;
  industry2: string;
  size: SizeCompanyEnum;
  description: string;
  positions: string[];
  contactDetailses: HostCompanyContactDetailsModel[];
}

export enum SizeCompanyEnum {
  _0 = 0,
  _1 = 1,
  _2 = 2,
  _3 = 3,
  _4 = 4,
  _5 = 5
}
