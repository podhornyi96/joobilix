export class HostCompanyAutoCompleteModel {
  companyId: string;
  companyName: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
}
