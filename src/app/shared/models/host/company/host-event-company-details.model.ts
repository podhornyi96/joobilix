import {RegistrationStatusEnum} from './host-event-company.model';
import {HostCompanyContactDetailsModel} from './host-company-contact-details.model';
import {SizeCompanyEnum} from './host-company-response.model';

export class HostEventCompanyDetailsModel {
  name: string;
  description: string;
  logoUrl: string;
  oneLiner: string;
  industry1: string;
  industry2: string;
  size: SizeCompanyEnum;
  positions: string[];
  contactDetailses: HostCompanyContactDetailsModel[];
  registrationStatus: RegistrationStatusEnum;
}
