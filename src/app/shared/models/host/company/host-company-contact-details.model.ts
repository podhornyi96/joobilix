import {BaseModel} from '../../base.model';

export class HostCompanyContactDetailsModel implements IHostCompanyContactDetails {
  fullName: string;
  emailAddress: string;
  phoneNumber: string;

  init(data?: any) {
    if (data) {
      this.fullName = data['fullName'];
      this.emailAddress = data['emailAddress'];
      this.phoneNumber = data['phoneNumber'];
    }
  }

  static fromJS(data: any): HostCompanyContactDetailsModel {
    let result = new HostCompanyContactDetailsModel();
    result.init(data);
    return result;
  }
}

export interface IHostCompanyContactDetails {
  fullName: string;
  emailAddress: string;
  phoneNumber: string;
}
