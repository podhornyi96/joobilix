import {HostCompanyModel} from './host-company.model';
import {BaseModel, IBaseModel} from '../../base.model';

export class HostCompanyPageResponseModel implements IBaseModel, IHostCompanyPageResponse {
  count: number;
  total: number;
  data: HostCompanyModel[];

  init(data?: any) {
    if (data) {
      this.count = data['count'];
      this.total = data['total'];
        if (data['data'] && data['data'].constructor === Array) {
          this.data = [];
          for (let item of data['data'])
            this.data.push(HostCompanyModel.fromJS(item));
        }
    }
  }

  static fromJS(data: any): any {
    let result = new HostCompanyPageResponseModel();
    result.init(data);
    return result;
  }
}

export interface IHostCompanyPageResponse {
  count: number;
  total: number;
  data: HostCompanyModel[];
}
