import {HostCompanyContactDetailsModel} from './host-company-contact-details.model';
import {BaseModel} from '../../base.model';

export class HostCompanyModel implements IHostCompany {
  id: string;
  logoUrl: string;
  name: string;
  contactDetailses: HostCompanyContactDetailsModel;

  init(data?: any) {
    if (data) {
      this.id = data['id'];
      this.logoUrl = data['logoUrl'];
      this.name = data['name'];
      if (data['contactDetailses']) {
        this.contactDetailses = HostCompanyContactDetailsModel.fromJS(data['contactDetailses']);
      }
    }
  }

  static fromJS(data: any): HostCompanyModel {
    let result = new HostCompanyModel();
    result.init(data);
    return result;
  }
}

export interface IHostCompany {
  id: string;
  logoUrl: string;
  name: string;
  contactDetailses: HostCompanyContactDetailsModel;
}
