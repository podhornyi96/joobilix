import {HostCompanyContactDetailsModel} from './host-company-contact-details.model';

export class HostEventCompanyModel implements IHostEventCompanyModel{
  companyId: string;
  logoUrl: string;
  name: string;
  numberOfPositions: number;
  registrationStatus: RegistrationStatusEnum;
  contactDetailses: HostCompanyContactDetailsModel;

  init(data?: any) {
    if (data) {
      this.companyId = data['companyId'];
      this.logoUrl = data['logoUrl'];
      this.name = data['name'];
      this.numberOfPositions = data['numberOfPositions'];
      this.registrationStatus = data['registrationStatus'];
      if (data['contactDetailses']) {
        this.contactDetailses = HostCompanyContactDetailsModel.fromJS(data['contactDetailses']);
      }
    }
  }

  static fromJS(data: any): HostEventCompanyModel {
    let result = new HostEventCompanyModel();
    result.init(data);
    return result;
  }
}

export interface IHostEventCompanyModel {
  companyId: string;
  logoUrl: string;
  name: string;
  numberOfPositions: number;
  registrationStatus: RegistrationStatusEnum;
  contactDetailses: HostCompanyContactDetailsModel;
}

export enum RegistrationStatusEnum {
  new = 0,
  failedToSendlvite = 1,
  inviteSent = 2,
  registrationCompleted = 3,
  deleted = 4,
  disagree = 5,
  agree = 6
}
