import {HostEvent} from './host-event.model';
import {IBaseModel} from '../../base.model';

export class HostEventsResponse implements IBaseModel, IHostEventsResponse {
  count: number;
  total: number;
  data: HostEvent[];

  init(data?: any) {
    if (data) {
      this.count = data['count'];
      this.total = data['total'];
        if (data['data'] && data['data'].constructor === Array) {
          this.data = [];
          for (let item of data['data'])
            this.data.push(HostEvent.fromJS(item));
        }
    }
  }

  static fromJS(data: any): HostEventsResponse {
    let result = new HostEventsResponse();
    result.init(data);
    return result;
  }
}

export interface IHostEventsResponse {
  count: number;
  total: number;
  data: HostEvent[];
}
