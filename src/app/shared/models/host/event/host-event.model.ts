import * as moment from 'moment';

export class HostEvent implements IHostEvent {
  id: string;
  eventStatus: EventStatusEnum;
  bgUrl: string;
  title: string;
  locationName: string;
  city: string;
  when:	moment.Moment;
  endDate: moment.Moment;

  init(data?: any) {
    if (data) {
      this.id = data['eventStatus'];
      this.eventStatus = data['eventStatus'];
      this.bgUrl = data['bgUrl'];
      this.title = data['title'];
      this.locationName = data['locationName'];
      this.city = data['city'];
      this.when = moment(data['when'].toString());
      this.endDate = moment(data['endDate'].toString());
    }
  }

  static fromJS(data: any): HostEvent {
    let result = new HostEvent();
    result.init(data);
    return result;
  }
}

export interface IHostEvent {
  id: string;
  eventStatus: EventStatusEnum;
  bgUrl: string;
  title: string;
  locationName: string;
  city: string;
  when:	moment.Moment;
  endDate: moment.Moment;
}

export enum EventStatusEnum {
  pending = 0,
  new = 1,
  suspended = 2,
  creatingCompleted = 3,
  deleted = 4,
  end = 5
}
