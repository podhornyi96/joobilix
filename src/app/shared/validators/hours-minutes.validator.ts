import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';
import {Directive} from '@angular/core';
import {DateHelper} from '../helpers/dateHelper';

@Directive({
  selector: '[minutesHoursValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MinutesHoursValidator,
      multi: true
    }
  ]
})
export class MinutesHoursValidator implements Validator {

  validator: ValidatorFn;

  registerOnValidatorChange(fn: () => void): void {
    this.validator = this.minutesHoursValidator();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.validator(c);
  }


  minutesHoursValidator(): ValidatorFn {
    return (c: FormControl) => {

      if (DateHelper.isValidHoursAndMinutes(c.value))
        return null;

      return {
        minutesHours: {
          valid: false
        }
      };
    };
  }

}
