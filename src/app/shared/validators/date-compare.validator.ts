import {Directive, Input} from '@angular/core';
import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';

import * as moment from 'moment';

export enum DateCompareOperation {
  GT,
  LT,
  GE,
  LE,
  EQ
}

@Directive({
  selector: '[dateCompareValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: DateCompareValidator,
      multi: true
    }
  ]
})
export class DateCompareValidator implements Validator {

  @Input() operation: DateCompareOperation;
  @Input() date1: string;
  @Input() date2: string;

  validator: ValidatorFn;

  registerOnValidatorChange(fn: () => void): void {
    this.validator = this.dateCompareValidator();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.validator(c);
  }


  dateCompareValidator(): ValidatorFn {
    return (c: FormControl) => {

      if (!c.value)
        return null;

      const hoursAndMinutes: string[] = c.value.split(':');

      if (!this.isValidMinutesAndHours(c.value)) {
        return;
      }

      const date = moment(this.date2);

      date.set('hour', +hoursAndMinutes[0]);
      date.set('minute', +hoursAndMinutes[1]);

      if (!this.compareDates(date)) {
        return null;
      }

      return {
        dateCompare: {
          valid: false
        }
      };
    };
  }

  private compareDates(date2: any): boolean {
    if (!moment(this.date1).isValid() || !date2.isValid()) {
      return null;
    }

    switch (this.operation) {
      case DateCompareOperation.EQ:
        return moment(this.date1).isSame(date2);
      case DateCompareOperation.LE:
        return moment(this.date1).isSameOrBefore(date2);
      case DateCompareOperation.GE:
        return moment(this.date1).isSameOrAfter(date2);
      case DateCompareOperation.GT:
        return moment(this.date1).isAfter(date2);
      case DateCompareOperation.LT:
        return moment(this.date1).isBefore(date2);
    }

    return true;
  }

  private isValidMinutesAndHours(value: string): boolean {
    const hoursAndMinutes: string[] = value.split(':');

    if (hoursAndMinutes.length !== 2 || isNaN(parseInt(hoursAndMinutes[0])) || isNaN(parseInt(hoursAndMinutes[1])) || value.indexOf('_') !== -1) {
      return false;
    }

    return true;
  }

}
