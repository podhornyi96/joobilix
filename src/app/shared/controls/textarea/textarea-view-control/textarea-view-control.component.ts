import {Component, forwardRef, Injector, Input, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ValidationErrors,
  Validator
} from '@angular/forms';

@Component({
  selector: 'jblx-textarea-view-control',
  templateUrl: './textarea-view-control.component.html',
  styleUrls: ['./textarea-view-control.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaViewControlComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => TextareaViewControlComponent),
      multi: true,
    }
  ]
})
export class TextareaViewControlComponent implements OnInit, ControlValueAccessor, Validator  {

  @Input() label: string;
  @Input() id: string = '';
  @Input() name: string = '';
  @Input() previewEnabled: boolean;
  @Input() textLength: number;
  @Input() isReadOnly: boolean;
  @Input() rows: number;
  @Input() cols: number;
  @Input() placeholder: string;

  ngControl: NgControl;

  string = String;

  private innerValue: any;

  private onTouchedCallback: () => void = () => {};
  private onChangeCallback: (_: any) => void = () => {};

  constructor(private inj: Injector) {}

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
  }

  get value(): any {
    return this.innerValue;
  };

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: any): void {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  registerOnValidatorChange(fn: () => void): void {}

  validate(c: AbstractControl): ValidationErrors | null {
    return null;
  }

}
