import {
  Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation
} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {InfoModalComponent} from '../../shared/modals/info-modal/info-modal.component';
import {HostService} from '../../host/host.service';
import {DeleteCompanyFromEventRequest} from '../../shared/models/host/company/host-delete-company-from-event-request.model';
import {ModalInfoSettingsDefaultConstants} from '../../shared/modals/info-modal/modal-info-settings-default.constants';

@Component({
  selector: 'jblx-remove-company-from-event',
  templateUrl: './remove-company-from-event.component.html',
  styleUrls: ['./remove-company-from-event.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class RemoveCompanyFromEventComponent implements OnInit {

  @Input() eventId: string;

  @Output() companyDeleted: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('content') modal: ElementRef;

  modalRef: NgbModalRef;
  companyName: string;
  model: DeleteCompanyFromEventRequest;

  constructor(private modalService: NgbModal, private hostService: HostService) {
    this.model = new DeleteCompanyFromEventRequest();
  }

  ngOnInit() {
  }

  show(companyId: string, companyName: string) {
    this.model.companyId = companyId;
    this.companyName = companyName;
    this.modalRef = this.modalService.open(this.modal, { centered: true, windowClass: 'company-remove-modal jblx-common-modal' });
  }

  remove() {
    this.hostService.deleteCompanyFromEvent(this.model, this.eventId)
      .subscribe(() => {
        this.companyDeleted.emit();
        this.close();
        const modalRef = this.modalService.open(InfoModalComponent, {centered: true, windowClass: 'jblx-common-modal'});
        const modalSettings = ModalInfoSettingsDefaultConstants.simpleInfo;
        modalSettings.title = `${this.companyName} was removed from this event`;
        modalRef.componentInstance.model = modalSettings;
      });
  }

  close() {
    this.model = new DeleteCompanyFromEventRequest();
    this.modalRef.close();
  }
}
