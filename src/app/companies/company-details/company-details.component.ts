import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import {HostCompanyResponseModel} from '../../shared/models/host/company/host-company-response.model';
import {HostService} from '../../host/host.service';
import {HostEventCompanyDetailsModel} from '../../shared/models/host/company/host-event-company-details.model';
import {RegistrationStatusEnum} from '../../shared/models/host/company/host-event-company.model';

@Component({
  selector: 'jblx-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.less']
})
export class CompanyDetailsComponent implements OnInit, OnDestroy {

  @Input() companyDetails: HostCompanyResponseModel | HostEventCompanyDetailsModel;
  isEventCompany: boolean;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private isGettingCompany = false;

  registrationStatus = RegistrationStatusEnum;

  constructor(private _hostApService: HostService) { }

  ngOnInit() {
    this.isEventCompany = this.companyDetails instanceof HostEventCompanyDetailsModel;
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
