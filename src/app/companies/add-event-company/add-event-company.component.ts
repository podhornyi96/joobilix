import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {HostAddCompanyToEventRequestModel} from '../../shared/models/host/company/host-add-company-to-event-request.model';
import {HostService} from '../../host/host.service';
import {HostCompanyAutoCompleteModel} from '../../shared/models/host/company/host-company-auto-complete.model';
import {Subject} from 'rxjs/Rx';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {FormHelper} from '../../shared/helpers/formHelper';

@Component({
  selector: 'jblx-add-event-company',
  templateUrl: './add-event-company.component.html',
  styleUrls: ['./add-event-company.component.less']
})
export class AddEventCompanyComponent implements OnInit, OnDestroy {

  @Input() eventId: string;
  @Output() companyCreated: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('content') modal: ElementRef;

  modalRef: NgbModalRef;
  company: HostAddCompanyToEventRequestModel;
  adminPhoneNumber: string;
  companyAutocomplete: HostCompanyAutoCompleteModel[];
  emailAutocomplete: string[];
  isSubmit = false;
  errorSubmit: number;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private modalService: NgbModal, private hostService: HostService) {
    this.company = new HostAddCompanyToEventRequestModel();
    this.companyAutocomplete = [];
    this.emailAutocomplete = [];
  }

  ngOnInit() {
  }

  show() {
    this.initCompanyAutocomplete();
    this.modalRef = this.modalService.open(this.modal,
      { centered: true, windowClass: 'add-event-company-modal jblx-common-modal' });
  }

  add(form: NgForm) {
    if (!FormHelper.isFormValid(form) || this.isSubmit) {
      return;
    }

    this.isSubmit = true;
    this.hostService.createEventCompany(this.company, this.eventId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.isSubmit = false;
        this.handlerCreateCompanyResponse(response, form);
      });
  }

  onEmailSelect(model) {
    const selectedAdmin = this.companyAutocomplete.find(c => c.emailAddress === model.item);
    if (selectedAdmin) {
      this.company.firstName = selectedAdmin.firstName;
      this.company.lastName = selectedAdmin.lastName;
    }
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.emailAutocomplete.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)));

  private initCompanyAutocomplete() {
    this.hostService.eventCompanyAutocomplete(this.eventId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.companyAutocomplete = response;
        this.emailAutocomplete = this.companyAutocomplete
          .map(c => c.emailAddress)
          .filter((value, index, self) => self.indexOf(value) === index); // distinct email
    });
  }

  private handlerCreateCompanyResponse(response, form: NgForm) {
    if (!response.errorCode) {
      this.companyCreated.emit();
      form.reset();
      this.close();
    } else {
      this.errorSubmit = response.errorCode;
    }
  }

  close() {
    // HACK: for fix padding right
    setTimeout(() => this.modalRef.close(), 0);
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
