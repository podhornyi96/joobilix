import {ViewChild} from '@angular/core';
import {HostCompanyModel} from '../../shared/models/host/company/host-company.model';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs/Rx';
import {SideModalComponent} from '../../shared/modals/side-modal/side-modal.component';
import {HostCompanyResponseModel} from '../../shared/models/host/company/host-company-response.model';
import {HostEventCompanyDetailsModel} from '../../shared/models/host/company/host-event-company-details.model';
import {HostEventCompanyModel} from '../../shared/models/host/company/host-event-company.model';

export class ListCompaniesBase {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('sideModal') sideModal: SideModalComponent;

  companies: MatTableDataSource<HostEventCompanyModel | HostCompanyModel>;
  displayedColumns: string[];
  pageSize = [200];
  selectedCompany: HostCompanyResponseModel | HostEventCompanyDetailsModel;

  unsubscribe$: Subject<boolean> = new Subject<boolean>();
  stepLoad = 50;
  skipColumnFilter: string[] = [];

  applyFilter(filterValue: string) {
    this.companies.filter = filterValue.trim().toLowerCase();
  }

  initMatTable() {
    this.initPaginator();
    this.initFilterPredicate();
    this.initSortAccessor();
    this.companies.sort = this.sort;
  }

  initPaginator() {
    if (!this.companies) {
      return;
    }

    this.companies.paginator = this.paginator;
  }

  initFilterPredicate() {
    if (!this.companies) {
      return;
    }

    this.companies.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm, key) => {
        if (this.skipColumnFilter.indexOf(key) >= 0) {
          return currentTerm;
        }
        return this.nestedFilterCheck(currentTerm, data, key);
      };

      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
  }

  initSortAccessor() {
    this.companies.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'contactName': return item.contactDetailses.fullName;
        default: return item[property];
      }
    };
  }

  nestedFilterCheck(search, data, key) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }

    return search;
  }

  unsubscribe() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
