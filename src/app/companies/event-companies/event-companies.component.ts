import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ListCompaniesBase} from '../base/list-companies-base';
import {MatTableDataSource} from '@angular/material';
import {HostEventCompanyModel, RegistrationStatusEnum} from '../../shared/models/host/company/host-event-company.model';
import {HostService} from '../../host/host.service';
import {HostEventCompanyDetailsModel} from '../../shared/models/host/company/host-event-company-details.model';
import {SideModalComponent} from '../../shared/modals/side-modal/side-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {InfoModalComponent} from '../../shared/modals/info-modal/info-modal.component';
import {ModalInfoSettingsDefaultConstants} from '../../shared/modals/info-modal/modal-info-settings-default.constants';
import {RemoveCompanyFromEventComponent} from '../remove-company-from-event/remove-company-from-event.component';
import {EventStatusEnum} from '../../shared/models/host/event/host-event.model';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'jblx-event-companies',
  templateUrl: './event-companies.component.html',
  styleUrls: ['./event-companies.component.less']
})
export class EventCompaniesComponent extends ListCompaniesBase implements OnInit, OnDestroy {

  @Input() set eventId(value: string) {
    this._eventId = value;

    this.getAllCompanies();
  }

  get eventId(): string {
    return this._eventId;
  }

  @Input() eventStatus: EventStatusEnum;

  @ViewChild('sideModal') sideModal: SideModalComponent;
  @ViewChild('removeCompany') removeCompanyModal: RemoveCompanyFromEventComponent;

  companies: MatTableDataSource<HostEventCompanyModel>;
  regStatus = RegistrationStatusEnum;

  private _eventId: string;

  constructor(private route: ActivatedRoute, private _hostApiService: HostService, private modalService: NgbModal) {
    super();
    this.displayedColumns = ['companyLogo', 'name', 'numberOfPositions', 'contactName', 'registrationStatus', 'actions'];
    this.skipColumnFilter = ['id', 'logoUrl'];
    this.selectedCompany = new HostEventCompanyDetailsModel();
  }

  get isNewEventEmpty(): boolean {
    var result = this.eventStatus === EventStatusEnum.new && (this.companies === undefined || this.companies.data.length === 0);
    return result;
  }

  ngOnInit() {

  }

  getAllCompanies(skip: number = 0, limit: number = 50, prevRequest: HostEventCompanyModel[] = []) {
    if (skip === 0) {
      this.companies = undefined;
    }

    this._hostApiService.getEventCompanies(skip, limit, this.eventId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data || !response.data.length) {
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepLoad;
          this.getAllCompanies(skip, limit, prevRequest);
        } else {
          prevRequest.sort((left, right) => {
            return ('' + left.name).localeCompare(right.name);
          });
          this.companies = new MatTableDataSource<HostEventCompanyModel>(prevRequest);
          this.initMatTable();
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  getClassByRegStatus(status: RegistrationStatusEnum): string {
    switch (status) {
      case RegistrationStatusEnum.new: return 'status-new';
      case RegistrationStatusEnum.inviteSent: return 'status-invite-sent';
      case RegistrationStatusEnum.registrationCompleted: return 'status-reg-completed';
      case RegistrationStatusEnum.failedToSendlvite: return 'status-failed-send';
      case RegistrationStatusEnum.deleted: return 'status-deleted';
      case RegistrationStatusEnum.disagree: return 'status-deleted';
      case RegistrationStatusEnum.agree: return 'status-reg-completed';
      default: return '';
    }
  }

  isCompanyDeleted(status: RegistrationStatusEnum): boolean {
    return status === RegistrationStatusEnum.deleted;
  }

  showCompanyDetails(selectedCompanyId: string, event: any): void {
    if (!selectedCompanyId) {
      return;
    }

    this._hostApiService.getEventCompany(this.eventId, selectedCompanyId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.selectedCompany = response;
        this.sideModal.show();
      });
  }

  sendReminderShow() {
    const modalRef = this.modalService.open(InfoModalComponent, {centered: true, windowClass: 'jblx-common-modal'});
    const modalSettings = ModalInfoSettingsDefaultConstants.confirmation;
    modalSettings.title = 'Send a reminder';
    modalSettings.message = 'By clicking “send”, we will send a reminder to all companies who haven’t opened their profile yet, and the ones with no positions.';
    modalSettings.actionBtnTxt = 'Send';
    modalSettings.actionCallBackFunc = () => {
      this._hostApiService.sendEventRemainder(this.eventId)
        .subscribe(() => {
          modalRef.close();
          const modalRefInfo = this.modalService.open(InfoModalComponent, {centered: true, windowClass: 'jblx-common-modal'});
          const modalInfoSettings = ModalInfoSettingsDefaultConstants.simpleInfo;
          modalInfoSettings.title = 'Reminder sent';
          modalInfoSettings.message = 'You will probably see more companies onboard soon!';
          modalRefInfo.componentInstance.model = modalInfoSettings;
        });
    };
    modalRef.componentInstance.model = modalSettings;
  }

  removeEventCompany(companyId: string, companyName: string, event: any) {
    event.stopPropagation();
    this.removeCompanyModal.show(companyId, companyName);
  }
}
