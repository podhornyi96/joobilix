import {Component, OnDestroy, OnInit} from '@angular/core';
import {HostCompanyModel} from '../../shared/models/host/company/host-company.model';
import {MatTableDataSource} from '@angular/material';
import {HostService} from '../../host/host.service';
import {ListCompaniesBase} from '../base/list-companies-base';

@Component({
  selector: 'jblx-host-list-companies',
  templateUrl: './host-list-companies.component.html',
  styleUrls: ['./host-list-companies.component.less']
})
export class HostListCompaniesComponent extends ListCompaniesBase implements OnInit, OnDestroy {

  companies: MatTableDataSource<HostCompanyModel>;

  constructor(private _hostApiService: HostService) {
    super();
    this.displayedColumns = [
      'companyLogo', 'name',
      'contactName', 'emailAddress'];
    this.skipColumnFilter = ['companyId', 'logoUrl'];
  }

  ngOnInit() {
    this.getAllCompanies();
  }

  getAllCompanies(skip: number = 0, limit: number = 50, prevRequest: HostCompanyModel[] = []) {
    if (skip === 0) {
      this.companies = undefined;
    }

    this._hostApiService.getHostCompanies(skip, limit)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        if (!response.data.length) {
          return;
        }

        prevRequest = prevRequest.concat(response.data);
        if (prevRequest.length < response.total) {
          skip += this.stepLoad;
          this.getAllCompanies(skip, limit, prevRequest);
        } else {
          response.data.sort((left, right) => {
            return ('' + left.name).localeCompare(right.name);
          });
          this.companies = new MatTableDataSource<HostCompanyModel>(response.data);
          this.initMatTable();
        }
      });
  }

  showCompanyDetails(selectedCompanyId: string): void {
    if (!selectedCompanyId) {
      return;
    }

    this._hostApiService.getCompanyById(selectedCompanyId)
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.selectedCompany = response;
        this.sideModal.show();
      });
  }

  ngOnDestroy() {
    this.unsubscribe();
  }
}
