import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HostIndexComponent} from './layouts/host-layout/host-index.component';
import {HeaderBarComponent} from './shared/layout/header-bar/header-bar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HostProfileComponent} from './profile/host-profile/host-profile.component';
import {ServiceModule} from './shared/services/service.module';
import {ExpiredDateDirective} from './shared/directives/expired-date.directive';
import {InputViewControlComponent} from './shared/controls/input/input-view-control/input-view-control.component';
import {TextMaskModule} from 'angular2-text-mask';
import {FormsModule} from '@angular/forms';
import {TextareaViewControlComponent} from './shared/controls/textarea/textarea-view-control/textarea-view-control.component';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import {EllipsisPipe} from './shared/pipes/ellipsis.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import {SideModalComponent} from './shared/modals/side-modal/side-modal.component';
import {CompanySizePipe} from './shared/pipes/enumConverters/company-size.pipe';
import {CompanyStatusRegisterPipe} from './shared/pipes/enumConverters/company-status-register.pipe';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './account/login/login.component';
import {HostService} from './host/host.service';
import {ApplyTokenInterceptor} from './shared/auth/apply-token.interceptor';
import {InfoModalComponent} from './shared/modals/info-modal/info-modal.component';
import {MinutesHoursValidator} from './shared/validators/hours-minutes.validator';
import {DateCompareValidator} from './shared/validators/date-compare.validator';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {IndentDirective} from './shared/directives/indent.directive';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {NgxMyDatePickerModule} from 'ngx-mydatepicker';
import {SiteLayoutComponent} from './layouts/site-layout/site-layout.component';
import {FooterComponent} from './shared/layout/footer/footer.component';
import {HomeComponent} from './home/home.component';
import {TermsOfUseComponent} from './shared/components/terms-of-use/terms-of-use.component';
import {PrivacyPolicyComponent} from './shared/components/privacy-policy/privacy-policy.component';
import {NguCarouselModule} from '@ngu/carousel';
import {LoginModalContentComponent} from './account/login/login-modal-content/login-modal-content.component';
import {RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';
import {ToastrModule} from 'ngx-toastr';
import {HostEventModalComponent} from './home/host-event-modal/host-event-modal.component';
import {HostEventModalContentComponent} from './home/host-event-modal/host-event-modal-content/host-event-modal-content.component';
import {environment} from '../environments/environment';
import {RecaptchaFormsModule} from 'ng-recaptcha/forms';
import {InternationalPhoneNumberModule} from 'ngx-international-phone-number';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';

import {PersonalInfoComponent} from './profile/personal-info/personal-info.component';
import {AccountService} from './account/account.service';
import {TermsModalComponent} from './shared/modals/terms-modal/terms-modal.component';

import {ResetPasswordModalComponent} from './account/reset-password-modal/reset-password-modal.component';
import {CustomFormsModule} from 'ngx-custom-validators';
import {ForgotPasswordComponent} from './account/forgot-password/forgot-password.component';
import {CompanyProfileComponent} from './profile/company-profile/company-profile.component';
import {OrganizationService} from './profile/organization.service';
import {CompanyIndexComponent} from './layouts/company-layout/company-index.component';
import {AboutComponent} from './profile/about/about.component';
import {PositionsComponent} from './positions/positions.component';
import {PositionsTableComponent} from './positions/positions-table/positions-table.component';
import {PositionDetailsComponent} from './positions/position-details/position-details.component';
import {RemovePositionModalComponent} from './positions/remove-position-modal/remove-position-modal.component';
import {AddPositionModalComponent} from './positions/add-position-modal/add-position-modal.component';
import {PositionComponent} from './positions/position/position.component';
import {HostEventComponent} from './events/event/host-event.component';
import {HostListCompaniesComponent} from './companies/list-companies/host-list-companies.component';
import {EventDescriptionComponent} from './events/description/event-description.component';
import {CreateEventComponent} from './events/create/create-event.component';
import {EventSliderComponent} from './events/event-slider/event-slider.component';
import {CompanyDetailsComponent} from './companies/company-details/company-details.component';
import {EventCompaniesComponent} from './companies/event-companies/event-companies.component';
import {RemoveCompanyFromEventComponent} from './companies/remove-company-from-event/remove-company-from-event.component';
import {AddEventCompanyComponent} from './companies/add-event-company/add-event-company.component';
import {EventService} from './events/event.service';
import {ManageAccountsComponent} from './profile/manage-accounts/manage-accounts.component';
import {AddContactModalComponent} from './profile/add-contact-modal/add-contact-modal.component';
import {AccountsTableComponent} from './profile/accounts-table/accounts-table.component';
import {DeclineInvitationComponent} from './events/decline-invitation/decline-invitation.component';
import {EventNewInvitationComponent} from './events/event-new-invitation/event-new-invitation.component';
import {CandidateComponent} from './events/candidates/candidate/candidate.component';
import {CandidatesComponent} from './events/candidates/candidates.component';
import {NouisliderModule} from 'ng2-nouislider';
import {CandidatesFilterComponent} from './events/candidates/candidates-filter/candidates-filter.component';
import {CandidateProfileComponent} from './events/candidates/candidate-profile/candidate-profile/candidate-profile.component';
import {RatingComponent} from './shared/components/rating/rating.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {DebounceDirective} from './shared/directives/debounce.directive';
import {CandidateFullProfileComponent} from './events/candidates/candidate-profile/candidate-full-profile/candidate-full-profile.component';
import {SlidingPanelComponent} from './shared/components/sliding-panel/sliding-panel.component';
import {LoaderComponent} from './shared/loader/loader.component';
import {LoaderService} from './shared/loader/loader.service';
import {CandidateRecruitmentWidgetComponent} from './events/candidates/candidate-profile/candidate-recruitment-widget/candidate-recruitment-widget.component';
import { CandidateInfoComponent } from './events/candidates/candidate-profile/candidate-info/candidate-info.component';
import { CandidateMainInfoComponent } from './events/candidates/candidate-profile/candidate-main-info/candidate-main-info.component';
import {UnauthErrorInterceptor} from './shared/auth/unauth-error.interceptor';
import {RefreshTokenInterceptor} from './shared/auth/refresh-token.interceptor';
import { SplitByLineDirective } from './shared/directives/split-by-line.directive';


@NgModule({
  declarations: [
    AppComponent,
    SiteLayoutComponent,
    HomeComponent,
    HostIndexComponent,
    HeaderBarComponent,
    HostEventComponent,
    HostListCompaniesComponent,
    HostProfileComponent,
    EventDescriptionComponent,
    LoginComponent,
    LoginModalContentComponent,
    HostEventModalContentComponent,
    CreateEventComponent,
    ExpiredDateDirective,
    InputViewControlComponent,
    TextareaViewControlComponent,
    EventSliderComponent,
    EllipsisPipe,
    SideModalComponent,
    HostEventModalComponent,
    CompanyDetailsComponent,
    CompanySizePipe,
    EventCompaniesComponent,
    CompanyStatusRegisterPipe,
    RemoveCompanyFromEventComponent,
    InfoModalComponent,
    AddEventCompanyComponent,
    IndentDirective,
    FooterComponent,

    PersonalInfoComponent,

    DateCompareValidator,
    MinutesHoursValidator,
    TermsOfUseComponent,
    PrivacyPolicyComponent,
    TermsModalComponent,
    ResetPasswordModalComponent,
    ForgotPasswordComponent,
    CompanyProfileComponent,
    PositionsComponent,
    CompanyIndexComponent,
    PositionComponent,
    PositionsTableComponent,
    AddPositionModalComponent,
    PositionDetailsComponent,
    AboutComponent,
    RemovePositionModalComponent,
    ManageAccountsComponent,
    AddContactModalComponent,
    AccountsTableComponent,
    DeclineInvitationComponent,
    EventNewInvitationComponent,
    CandidateComponent,
    CandidatesComponent,
    CandidatesFilterComponent,
    CandidateProfileComponent,
    CandidateFullProfileComponent,
    RatingComponent,
    DebounceDirective,
    SlidingPanelComponent,
    LoaderComponent,
    CandidateRecruitmentWidgetComponent,
    CandidateInfoComponent,
    CandidateMainInfoComponent,
    SplitByLineDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    LoadingBarModule.forRoot(),
    Ng4GeoautocompleteModule.forRoot(),
    NgxMyDatePickerModule.forRoot(),
    RecaptchaModule.forRoot(),
    ToastrModule.forRoot(),
    ScrollToModule.forRoot(),
    NouisliderModule,
    PerfectScrollbarModule,
    ServiceModule,
    TextMaskModule,
    FormsModule,
    RecaptchaFormsModule,
    NgxPageScrollModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    NguCarouselModule,
    InternationalPhoneNumberModule,
    CustomFormsModule
  ],
  providers: [
    EllipsisPipe,
    LoaderService,
    EventService,
    HostService,
    AccountService,
    OrganizationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApplyTokenInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: UnauthErrorInterceptor,
      multi: true },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {siteKey: environment.recapchaGlobalKey} as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    InfoModalComponent,
    LoginModalContentComponent,
    HostEventModalContentComponent,
    ForgotPasswordComponent
  ],
})
export class AppModule {
}
