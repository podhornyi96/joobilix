import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ForgotPasswordRequest} from '../models/forgot-password-request';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {Subject} from 'rxjs/Rx';
import {ModalInfoSettingsDefaultConstants} from '../../shared/modals/info-modal/modal-info-settings-default.constants';
import {InfoModalComponent} from '../../shared/modals/info-modal/info-modal.component';
import {ToastrService} from 'ngx-toastr';
import {AccountService} from '../account.service';

@Component({
  selector: 'jblx-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  model: ForgotPasswordRequest;
  isSubmit = false;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(public activeModal: NgbActiveModal,
              private modalService: NgbModal,
              private accountService: AccountService,
              private toastr: ToastrService) {
    this.model = new ForgotPasswordRequest();
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (!FormHelper.isFormValid(form) || this.isSubmit) {
      return;
    }

    this.isSubmit = true;
    this.accountService
      .postResetPassword(this.model)
      .takeUntil(this.unsubscribe$)
      .subscribe(() => {
        this.isSubmit = false;
        this.activeModal.close();
        const modalRefInfo = this.modalService.open(InfoModalComponent, {windowClass: 'jblx-common-modal'});
        const modalInfoSettings = ModalInfoSettingsDefaultConstants.simpleInfo;
        modalInfoSettings.title = 'Got It';
        modalInfoSettings.message = 'If there\'s a Joobilix account linked to this email address, we\'ll send over instructions to reset your password.';
        modalRefInfo.componentInstance.model = modalInfoSettings;
      }, (error) => {
        this.isSubmit = false;
        this.toastr.error('Invalid user name or password.');
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
