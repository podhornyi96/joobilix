import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ChangePasswordRequest} from '../models/change-password-request';
import {ToastrService} from 'ngx-toastr';
import {Subject} from 'rxjs/Rx';
import {LoginResponse} from '../login/login-response.model';
import {LocalStorageKeysConstant} from '../../shared/constants/local-storage-keys.constant';
import {AccountService} from '../account.service';

@Component({
  selector: 'jblx-reset-password-modal',
  templateUrl: './reset-password-modal.component.html',
  styleUrls: ['./reset-password-modal.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class ResetPasswordModalComponent implements AfterViewInit, OnInit, OnDestroy {
  @Input() isOpenResetPassword: boolean;

  @ViewChild('content') modal: ElementRef;

  changePasswordModel: ChangePasswordRequest;
  isSubmit: boolean;
  modalRef: NgbModalRef;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private _modalService: NgbModal, private router: Router,
              private accountService: AccountService,
              private toastr: ToastrService, private activeRoute: ActivatedRoute) {
    this.changePasswordModel = new ChangePasswordRequest();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      const session = params['session'];
      this.setSession(session);
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.isOpenResetPassword) {
        this.show();
      }
    }, 0);
  }

  onSubmit(form: NgForm) {
    if (!FormHelper.isFormValid(form) || this.isSubmit) {
      return;
    }

    this.isSubmit = true;
    this.accountService
      .putResetPassword(this.changePasswordModel)
      .takeUntil(this.unsubscribe$)
      .subscribe(() => {
        this.accountService.logoutUser().subscribe(() => {
            this.isSubmit = false;
            this.router.navigateByUrl('/');
            this.modalRef.close();
          });
      }, (error) => {
        this.isSubmit = false;
        this.toastr.error('Invalid user name or password.');
      });
  }

  show() {
    this.modalRef = this._modalService.open(this.modal,
      { windowClass: 'reset-password-modal jblx-common-modal' });
  }

  private setSession(session: string) {
    const loginResponse = new LoginResponse();
    loginResponse.accessToken = session;
    localStorage.setItem(LocalStorageKeysConstant.CurrentUser, JSON.stringify(loginResponse));
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
