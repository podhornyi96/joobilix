import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {ProfileInfo} from '../shared/models/host/profile/profile-info.model';

@Injectable()
export class AccountInteractionService {
  private updatedProfileSource = new Subject<void>();
  private refreshedProfileInfoSource = new Subject<ProfileInfo>();

  updatedProfile$ = this.updatedProfileSource.asObservable();
  refreshedProfileInfo$ = this.refreshedProfileInfoSource.asObservable();

  updatedProfile() {
    this.updatedProfileSource.next();
  }

  refreshedProfileInfo(profileInfo: ProfileInfo) {
    this.refreshedProfileInfoSource.next(profileInfo);
  }
}
