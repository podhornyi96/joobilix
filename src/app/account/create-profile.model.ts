export class CreateProfileRequest {
  firstName: string;
  lastName: string;
  countryPhoneNumberPrefix: string;
  phoneNumber: string;
  jobTitle: string;
}
