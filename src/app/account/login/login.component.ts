import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HostService } from '../../host/host.service';
import { LoginModalContentComponent } from './login-modal-content/login-modal-content.component';


@Component({
  selector: 'login-modal',
  template: '',
  styleUrls: ['./login.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {

  private modalRef: NgbModalRef;

  constructor(private modalService: NgbModal, private hostService: HostService, private router: Router) {
  }

  openModal() {
    this.modalRef = this.modalService.open(LoginModalContentComponent, { size: 'lg'});

    this.modalRef.componentInstance.modalClosed.subscribe(x => {
      this.modalRef.close();
    });
  }

}
