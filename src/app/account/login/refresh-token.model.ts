export class RefreshTokenRequest {

  sessionToken: string;
  refreshToken: string;

  constructor(obj?: Partial<RefreshTokenRequest>) {
    Object.assign(this, obj);
  }

}
