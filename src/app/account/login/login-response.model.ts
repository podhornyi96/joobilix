
export class LoginResponse {
  accessToken: string;
  refreshToken: string;
  expires: number;
  capchaFailed: boolean;
  expirationDate: Date;
}
