import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormHelper} from '../../../shared/helpers/formHelper';
import {HostService} from '../../../host/host.service';
import {LoginResponse} from '../login-response.model';
import {LoginModel} from '../login.model';
import {AccountInteractionService} from '../../account-interaction.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ForgotPasswordComponent} from '../../forgot-password/forgot-password.component';
import {AccountService} from '../../account.service';
import {CurrentProfile} from '../../current-profile.model';
import {RedirectHelper} from '../../../shared/helpers/redirectHelper';

@Component({
  selector: 'jblx-login-modal-content',
  templateUrl: './login-modal-content.component.html'
})
export class LoginModalContentComponent implements OnInit {

  @ViewChild(NgForm) loginForm: NgForm;
  @ViewChild('captchaRef') captchaRef: any;

  @Output() modalClosed: EventEmitter<void> = new EventEmitter<void>();

  loginModel: LoginModel = new LoginModel();

  submitted = false;

  constructor(
    private toastr: ToastrService,
    private hostService: HostService,
    private accountService: AccountService,
    private router: Router,
    private accountInteractionService: AccountInteractionService,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  onSubmit(): void {
    this.submitted = true;

    if (!FormHelper.isFormValid(this.loginForm)) {
      return;
    }

    this.captchaRef.execute();
  }

  public submit(captchaResponse: string): void {
    this.loginModel.capchaSession = captchaResponse;

    this.accountService.login(this.loginModel).toPromise().then((loginResponse: LoginResponse) => {
      if (!loginResponse || String.isNullOrEmpty(loginResponse.accessToken)) {
        this.loginFail();
        return;
      }

      this.accountService.setSession(loginResponse);

      this.accountInteractionService.updatedProfile();

      this.modalClosed.next();

      this.redirectAfterLogin();
    }, (error) => {
      this.loginFail();
    });
  }

  private loginFail() {
    this.captchaRef.reset();
    this.toastr.error('Invalid user name or password.');
    this.submitted = false;
  }

  public forgotPasswordModalShow() {
    this.activeModal.close();

    // HACK: for fix padding right
    setTimeout(() =>
        this.modalService.open(ForgotPasswordComponent, {windowClass: 'jblx-common-modal forgot-password-modal', size: 'lg'}),
      0);
  }

  private redirectAfterLogin(): void {
    this.accountService.getCurrentProfile().subscribe((profile: CurrentProfile) => {
      const redirectUrl = RedirectHelper.getLoginFlowRedirectUrl(profile);

      this.router.navigate([redirectUrl]);
    });
  }
}
