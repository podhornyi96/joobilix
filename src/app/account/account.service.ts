import 'rxjs/add/operator/map';

import {Injectable} from '@angular/core';
import {LocalStorageKeysConstant} from '../shared/constants/local-storage-keys.constant';
import {Observable} from 'rxjs/Observable';
import {ProfileInfo} from '../shared/models/host/profile/profile-info.model';
import {AccountInteractionService} from './account-interaction.service';
import {HostProfileDataResponse} from '../shared/models/host/profile/host-profile-data-response.model';
import {HostService} from '../host/host.service';
import {CreateProfileRequest} from './create-profile.model';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {CurrentProfile} from './current-profile.model';
import {LoginResponse} from './login/login-response.model';
import {ForgotPasswordResponse} from './models/forgot-password-response';
import {ChangePasswordRequest} from './models/change-password-request';
import {ForgotPasswordRequest} from './models/forgot-password-request';
import {LoginModel} from './login/login.model';
import {Router} from '@angular/router';
import {RefreshTokenRequest} from './login/refresh-token.model';
import {PathConstants} from '../shared/helpers/path.constants';
import * as moment from 'moment';

@Injectable()
export class AccountService {

  private currentProfile: CurrentProfile;

  constructor(
    private _hostApiService: HostService,
    private router: Router,
    private _accountInteractionService: AccountInteractionService,
    private http: HttpClient) {
    _accountInteractionService.updatedProfile$.subscribe(() => {
      this.refreshUserAccount()
        .subscribe((result) => this._accountInteractionService.refreshedProfileInfo(result));
    });
  }

  login(loginModel: LoginModel): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${environment.apiUrl}/api/v2/Accounts/Login`, loginModel).do(() => {
      this.currentProfile = null;
    });
  }

  putResetPassword(changePassword: ChangePasswordRequest): Observable<void> {
    return this.http.put<void>(`${environment.apiUrl}/api/v2/Accounts/Password`, changePassword);
  }

  postResetPassword(forgotPassword: ForgotPasswordRequest): Observable<ForgotPasswordResponse> {
    return this.http.post<ForgotPasswordResponse>(`${environment.apiUrl}/api/v2/Accounts/Password`, forgotPassword);
  }

  refreshToken(refreshTokenRequest: RefreshTokenRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${environment.apiUrl}/api/v2/Accounts/Token`, refreshTokenRequest);
  }

  getUserAccount(): Observable<ProfileInfo> {
    const profileInfoRaw = localStorage.getItem(LocalStorageKeysConstant.UserProfileInfo);

    if (profileInfoRaw) {
      const result = ProfileInfo.fromJS(JSON.parse(profileInfoRaw));
      return Observable.of(result);
    } else {
      return this.refreshUserAccount();
    }
  }

  refreshUserAccount(): Observable<ProfileInfo> {
    // HACK: need refactoring;
    return this.getCurrentProfile(true)
      .flatMap(() => this._hostApiService.getHostProfile()
      .map(d => {
        const profileInfo = this.refreshLocalStorage(d);
        return profileInfo;
      })).first();
  }

  createProfile(createProfileRequest: CreateProfileRequest): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v2/Accounts/Profile`, createProfileRequest);
  }

  getCurrentProfile(isSkipExistProfile: boolean = false): Observable<CurrentProfile> {
    if (!isSkipExistProfile && this.currentProfile) {
      return Observable.of(this.currentProfile);
    }

    return this.http.get<CurrentProfile>(`${environment.apiUrl}/api/v2/Accounts/Current`).do((data) => {
      this.currentProfile = data;
    });
  }

  agreeTerms(): Observable<void> {
    return this.http.put<void>(`${environment.apiUrl}/api/v2/Legal/TermsAndPrivacy`, {});
  }

  isLoginStepsDone(nextUrl: string): boolean {
    if (!this.currentProfile) {
      return false;
    }

    if ((this.currentProfile.needToSetProfile || this.currentProfile.needToAgreeTerms)) {
      const result = PathConstants.setProfileAndAgreeTerms.endsWith(nextUrl);
      return result;
    }

    if (this.currentProfile.needToSetOrganizationProfile) {
      const result = PathConstants.setOrganizationProfile.endsWith(nextUrl);
      return result;
    }

    return true;
  }

  getLoginData(): LoginResponse {
    const loginData: string = localStorage.getItem(LocalStorageKeysConstant.CurrentUser);

    if (!loginData || '{}' === loginData) {
      return null;
    }

    return JSON.parse(loginData);
  }

  isTokenExpired(): boolean {
    const loginData = this.getLoginData();

    if (!loginData) {
      return true;
    }

    console.log('Current data: ' + moment().toDate());
    console.log('Expiration date: ' + moment(loginData.expirationDate).toDate());

    return moment(loginData.expirationDate).isBefore(moment());
  }

  isNeedRefreshToken(): boolean {
    const loginData = this.getLoginData();
    if (!loginData || !loginData.expirationDate) {
      return false;
    }

    const expirationTimestamp = new Date(loginData.expirationDate).getTime() - 15000;
    const isExpired = Date.now() > expirationTimestamp; // - 15 second;
    return isExpired;
  }

  setSession(loginData: LoginResponse): void {
    if (!loginData) {
      return;
    }

    loginData.expirationDate = moment().add(loginData.expires, 'seconds').toDate();

    localStorage.setItem(LocalStorageKeysConstant.CurrentUser, JSON.stringify(loginData));
  }

  isLoggedIn(): boolean {
    const loginDataJson: string = localStorage.getItem(LocalStorageKeysConstant.CurrentUser);

    if (String.isNullOrEmpty(loginDataJson)) {
      return false;
    }

    const currentUserData: LoginResponse = JSON.parse(loginDataJson);

    return !String.isNullOrEmpty(currentUserData.accessToken);
  }

  private refreshLocalStorage(profile: HostProfileDataResponse): ProfileInfo {
    const profileInfo = new ProfileInfo(profile.firstName, profile.lastName, profile.logoUrl);
    localStorage.setItem(LocalStorageKeysConstant.UserProfileInfo, JSON.stringify(profileInfo));
    return profileInfo;
  }

  logoutUser(isRedirectHome: boolean = true): Observable<any> {
    localStorage.clear();

    this.currentProfile = null;
    this._accountInteractionService.refreshedProfileInfo(null);

    if (isRedirectHome) {
      this.router.navigate(['/home']);
    }

    return Observable.of(true);
  }

}
