export class CurrentProfile {
  firstName: string;
  lastName: string;
  emailAddress: string;
  roles: string[];
  needToSetProfile: boolean;
  needToAgreeTerms: boolean;
  needToSetOrganizationProfile: boolean;

}
