import {OrganizationProfile} from './organization-profile.model';

export class OrganizationProfileUpdateRequest extends OrganizationProfile {
  imageData: string;
}
