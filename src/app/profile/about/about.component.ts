import {Component, OnInit} from '@angular/core';
import {OrganizationProfile} from '../organization-profile.model';
import {OrganizationService} from '../organization.service';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.less']
})
export class AboutComponent implements OnInit {

  profile: OrganizationProfile = new OrganizationProfile();

  string = String;

  comapnySizes: any = [
    {id: 0, size: '1-10'},
    {id: 1, size: '10-50'},
    {id: 2, size: '50-100'},
    {id: 3, size: '100-500'},
    {id: 4, size: '500-1000'},
    {id: 5, size: '1000+'}
  ];

  constructor(public organizationService: OrganizationService, private spinner: LoaderService) {
  }

  ngOnInit() {
    this.spinner.start();

    this.organizationService.getProfile().subscribe((resp: OrganizationProfile) => {
      this.profile = resp;

      this.spinner.complete();
    });
  }

}
