export class AccountSummaryModel {
  firstName: string;
  lastName: string;
  emailAddress: string;
  countryPhonePrefix: string;
  phoneNumber: string;
  jobTitle: string;
  status: number;
}
