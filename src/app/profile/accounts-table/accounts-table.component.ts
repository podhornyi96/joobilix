import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AccountSummaryModel} from './account-summary.model';
import {AccountTableDataModel} from './account-table-data.model';

@Component({
  selector: 'jblx-accounts-table',
  templateUrl: './accounts-table.component.html',
  styleUrls: ['./accounts-table.component.less']
})
export class AccountsTableComponent implements OnInit {

  @Input() data: AccountTableDataModel;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  model: MatTableDataSource<AccountSummaryModel>;
  displayedColumns: string[] = ['name', 'title', 'emailAddress', 'phoneNumber', 'status'];
  pageSize = [30];

  registrationStatuses: any = {
    0: 'New',
    1: 'Failed to invite',
    2: 'Invited',
    3: 'Registered',
    4: 'Declined',
    5: 'Disagree',
    6: 'Agree'
  };

  string = String;

  constructor() {
  }

  ngOnInit() {
    this.initMatTable();
  }

  initMatTable() {
    this.model = new MatTableDataSource<AccountSummaryModel>(this.data.accounts);
    this.initPaginator();
    this.model.sort = this.sort;
  }

  initPaginator() {
    if (!this.model) {
      return;
    }

    this.model.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.model.filter = filterValue.trim().toLowerCase();
  }

  getStatusClass(status: number): string {
    switch (status) {
      case 0: return '';
      case 1: return 'failed';
      case 2: return 'invited';
      case 3: return 'registered';
      case 4: return 'failed';
    }
  }

  phoneNumberFormatter(phone: string, phonePrefix: string): string {
    if (!phone) {
      return '';
    }

    const result = phonePrefix ? `(${phonePrefix})-` : '';
    return result + phone;
  }

}
