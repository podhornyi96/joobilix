import {AccountSummaryModel} from './account-summary.model';

export class AccountTableDataModel {
  accounts: AccountSummaryModel[];
  onClickRowHandler: (model: AccountSummaryModel) => void;
}
