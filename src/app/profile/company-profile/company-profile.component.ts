import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {OrganizationService} from '../organization.service';
import {OrganizationProfile} from '../organization-profile.model';
import {NgForm} from '@angular/forms';
import {OrganizationProfileUpdateRequest} from '../organization-profile-update-request.model';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationHelper} from '../../shared/helpers/locationHelper';
import {FormHelper} from '../../shared/helpers/formHelper';
import {FileHelper} from '../../shared/helpers/fileHelper';
import {LocationModel} from '../../events/event/event.model';
import {LoaderService} from '../../shared/loader/loader.service';
import {AccountInteractionService} from '../../account/account-interaction.service';
import {AccountService} from '../../account/account.service';

@Component({
  selector: 'company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class CompanyProfileComponent implements OnInit {

  @ViewChild('profileForm') profileForm: NgForm;

  profile: OrganizationProfileUpdateRequest = new OrganizationProfileUpdateRequest();
  locationInvalid = false;

  userSettings = {
    showRecentSearch: false,
    showCurrentLocation: false,
    showSearchButton: false,
    inputPlaceholderText: 'location',
    inputString: ''
  };

  comapnySizes: any = [
    {id: 0, size: '1-10'},
    {id: 1, size: '10-50'},
    {id: 2, size: '50-100'},
    {id: 3, size: '100-500'},
    {id: 4, size: '500-1000'},
    {id: 5, size: '1000+'}
  ];

  submitted = false;
  isRepresentative = false;
  isEdit = false;
  imagePreview: any;

  constructor(public organizationService: OrganizationService, private router: Router, private route: ActivatedRoute,
              private spinner: LoaderService, private accountService: AccountService,
              private _accountInteractionService: AccountInteractionService) {
  }

  ngOnInit() {
    this.spinner.start();

    this.isEdit = (this.route.snapshot.queryParamMap.get('edit') === 'true');

    this.organizationService.getProfile().subscribe((resp: OrganizationProfile) => {
      this.profile = resp as OrganizationProfileUpdateRequest;

      if (this.profile.location) {
        this.userSettings.inputString = this.profile.location.placeName;

        // according to documentation to update location
        this.userSettings = Object.assign({}, this.userSettings);
      }

      this.spinner.complete();
    });
  }

  readURL(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      const file = fileInput.target.files[0];
      reader.onload = (e) => {
        this.imagePreview = reader.result;
      };

      reader.readAsDataURL(file);
    }
  }

  autoCompleteCallback(response: any) {
    const selectedLocation: LocationModel = LocationHelper.getLocationFromResponse(response);

    if (!selectedLocation) {
      return;
    }

    this.profile.location = selectedLocation;
    this.checkLocation();
  }

  // required, longitude, latitude, country, city
  // return true if invalid
  private checkLocation(): boolean {
    this.locationInvalid = !(this.profile.location
      && this.profile.location.city
      && this.profile.location.country
      && this.profile.location.latitude
      && this.profile.location.longitude);

    return this.locationInvalid;
  }

  private stopSubmitted() {
    this.spinner.complete();
    this.submitted = false;
  }

  lengthFormatter(text: string): number {
    if (!text) {
      return 0;
    }

    return text.length;
  }

  async onSubmit() {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    const isLocationInvalid = this.checkLocation();
    if (!FormHelper.isFormValid(this.profileForm) || isLocationInvalid) {
      this.stopSubmitted();
      return;
    }

    this.spinner.start();
    if (this.imagePreview) {
      this.profile.imageData = this.imagePreview.split(',').pop();
    } else if (!this.profile.logoUrl) {
      this.profile.imageData = await FileHelper.convertToDataURLviaCanvas('/assets/images/company-image-default.png', 'image/png');
    }

    this.organizationService.updateProfile(this.profile).subscribe(() => {
      // TODO: add here welcome to joobilix if create
      this.accountService.refreshUserAccount()
        .finally(() => this.stopSubmitted())
        .subscribe((result) => {
        this._accountInteractionService.refreshedProfileInfo(result);
        this.router.navigate(['/company/about']);
      });
    });
  }

}
