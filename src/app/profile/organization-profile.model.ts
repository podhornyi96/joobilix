import {LocationModel} from '../events/event/event.model';


export class OrganizationProfile {
  name: string;
  oneLiner: string;
  about: string;
  industry1: string;
  industry2: string;
  logoUrl: string;
  size: number;
  location: LocationModel;
  whyWeAreTheBest: string;
  accessibleOffices: boolean;
  wishToCreateDiverctyWorkForce: boolean;
}
