import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {OrganizationProfile} from './organization-profile.model';
import {OrganizationProfileUpdateRequest} from './organization-profile-update-request.model';
import {InviteColleagueModel} from './add-contact-modal/invite-colleague.model';
import {InviteColleagueResponseModel} from './add-contact-modal/invite-colleague-response.model';
import {AccountSummaryModel} from './accounts-table/account-summary.model';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  private fields: string[] = ['Biz Dev', 'Consulting', 'Customer service', 'Design', 'Development', 'Engineering', 'Finance & Accounting', 'Human Resources', 'Legal', 'Marketing', 'Operations', 'Product', 'Sales', 'Strategy & Planning', 'UX-UI', 'Other'];
  private companyFields: string[] = ['Accounting, Finance & Banking', 'Aerospace & Aviation', 'Agriculture & Environments', 'Architecture & Planning', 'Automotive & Transportation', 'Biology & Chemistry', 'Communications & electronics', 'Computer & Network Security', 'Computer Hardware', 'Consulting', 'Construction', 'Consumer Goods', 'Consumer Services', 'Design', 'Education', 'Energy & Natural Resources', 'Events Services', 'Facilities Services', 'Fashion, Apparel & Arts', 'Food & Beverages', 'Games, Gambling & Casinos', 'Government & Security', 'Health & Medicine', 'Human Resources', 'Human rights & Social Organization', 'Insurance', 'International Trade and Development', 'Internet (Aidan Taylor Marketing Competency)', 'Law', 'Logistics and Supply Chain', 'Machinery', 'Marketing and Advertising', 'Mechanical or Industrial Engineering', 'Media & Entertainment', 'Nanotechnology', 'Packaging and Containers', 'Real Estate', 'Retail', 'Semiconductors', 'Sports', 'Telecommunications', 'Travel, Tourism & Leisure', 'Other'
];

  constructor(private http: HttpClient) {
  }

  getProfile(): Observable<OrganizationProfile> {
    return this.http.get<OrganizationProfile>(`${environment.apiUrl}/api/v2/Organization/Profile`);
  }

  updateProfile(input: OrganizationProfileUpdateRequest): Observable<void> {
    return this.http.put<void>(`${environment.apiUrl}/api/v2/Organization/Profile`, input);
  }

  invite(input: InviteColleagueModel): Observable<InviteColleagueResponseModel> {
    return this.http.post<InviteColleagueResponseModel>(`${environment.apiUrl}/api/v2/Organization/Accounts/Invite`, input);
  }

  getAccounts(): Observable<AccountSummaryModel[]> {
    return this.http.get<AccountSummaryModel[]>(`${environment.apiUrl}/api/v2/Organization/Accounts`);
  }

  getFields(): string[] {
    return this.fields;
  }

  getCompanyFields(): string[] {
    return this.companyFields;
  }
}
