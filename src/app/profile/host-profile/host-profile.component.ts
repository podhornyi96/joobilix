import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HostProfileDataResponse} from '../../shared/models/host/profile/host-profile-data-response.model';
import {Subject} from 'rxjs/Rx';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {UpdateProfileRequest} from '../../shared/models/host/profile/update-profile-request.model';
import {AccountInteractionService} from '../../account/account-interaction.service';
import {HostService} from '../../host/host.service';
import {ObjectHelper} from '../../shared/helpers/objectHelper';
import {PhoneNumberComponent} from 'ngx-international-phone-number';

@Component({
  selector: 'jblx-host-profile',
  templateUrl: './host-profile.component.html',
  styleUrls: ['./host-profile.component.less']
})
export class HostProfileComponent implements OnInit, OnDestroy {

  @ViewChild('phoneNumberComponent') phoneNumber: PhoneNumberComponent;

  profile: HostProfileDataResponse;
  originalUpdateProfileRequest: UpdateProfileRequest;
  currentUpdateProfileRequest: UpdateProfileRequest;
  isViewMode: boolean;
  isSubmit = false;
  imagePreview: any;

  private unsubscribe$: Subject<boolean> = new Subject<boolean>();

  get fullPhone(): string {
    return [this.profile.countryPhoneNumberPrefix, this.profile.phoneNumber].filter(d => d).join('');
  }

  set fullPhone(value: string) {
    if (!value) {
      this.currentUpdateProfileRequest.phoneNumber = undefined;
      this.currentUpdateProfileRequest.countryPhoneNumberPrefix = undefined;
      return;
    }

    if (!this.phoneNumber.selectedCountry) {
      this.currentUpdateProfileRequest.phoneNumber = value;
      return;
    }

    const index = value.indexOf(this.phoneNumber.selectedCountry.dialCode);
    if (index < 0) {
      this.currentUpdateProfileRequest.phoneNumber = value;
      this.currentUpdateProfileRequest.countryPhoneNumberPrefix = undefined;
    } else {
      this.currentUpdateProfileRequest.countryPhoneNumberPrefix = `+${this.phoneNumber.selectedCountry.dialCode}`;
      this.currentUpdateProfileRequest.phoneNumber = value.substring(index + this.phoneNumber.selectedCountry.dialCode.length);
    }

    console.log(this.currentUpdateProfileRequest.countryPhoneNumberPrefix);
    console.log(this.currentUpdateProfileRequest.phoneNumber);
  }

  constructor(
    private _hostService: HostService,
    private _activeRoute: ActivatedRoute,
    private _router: Router,
    private _accountInteractionService: AccountInteractionService) {
    this.isViewMode = !(_activeRoute.snapshot.url[1] && _activeRoute.snapshot.url[1].path === 'edit');
  }

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this._hostService.getHostProfile()
      .takeUntil(this.unsubscribe$)
      .subscribe((response) => {
        this.initObjects(response);
      });
  }

  save(form: NgForm) {
    if (!FormHelper.isFormValid(form) || this.isSubmit) {
      return;
    }

    if (ObjectHelper.areObjectEquals(this.originalUpdateProfileRequest, this.currentUpdateProfileRequest)) {
      this.profileUpdateCompleted();
    }

    this.isSubmit = true;
    this.currentUpdateProfileRequest.imageData = this.imagePreview;
    this._hostService.updateProfile(this.currentUpdateProfileRequest)
      .finally(() => this.isSubmit = false)
      .subscribe((response) => {
        this.profileUpdateCompleted();
      });
  }

  readURL(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      let reader = new FileReader();
      let file = fileInput.target.files[0];
      reader.onload = (e) => {
        this.imagePreview = reader.result;
      };

      reader.readAsDataURL(file);
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  private profileUpdateCompleted(): void {
    this._accountInteractionService.updatedProfile();
    this._router.navigate(['/host/profile']);
  }

  private initObjects(profileData: HostProfileDataResponse): void {
    this.profile = profileData;
    this.originalUpdateProfileRequest = UpdateProfileRequest.fromHostProfileDataResponse(this.profile);
    this.currentUpdateProfileRequest = JSON.parse(JSON.stringify(this.originalUpdateProfileRequest));
  }

}
