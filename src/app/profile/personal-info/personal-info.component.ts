import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {CreateProfileRequest} from '../../account/create-profile.model';
import {CurrentProfile} from '../../account/current-profile.model';
import {FormHelper} from '../../shared/helpers/formHelper';
import {TermsModalComponent} from '../../shared/modals/terms-modal/terms-modal.component';
import {AccountService} from '../../account/account.service';
import {RedirectHelper} from '../../shared/helpers/redirectHelper';

@Component({
  selector: 'personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.less'],
})
export class PersonalInfoComponent implements OnInit {

  @ViewChild(NgForm) createProfileForm: NgForm;
  @ViewChild(TermsModalComponent) termsModal: TermsModalComponent;

  createProfileRequest: CreateProfileRequest = new CreateProfileRequest();
  currentProfile: CurrentProfile;

  submitted = false;

  phoneTextMask: any = {
    mask: ['+', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    guide: false
  };

  constructor(private accountService: AccountService, private router: Router) {
  }

  ngOnInit() {
    this.accountService.getCurrentProfile().subscribe((currentProfile: CurrentProfile) => {
      this.currentProfile = currentProfile;

      this.createProfileRequest.firstName = currentProfile.firstName;
      this.createProfileRequest.lastName = currentProfile.lastName;

      if (!currentProfile.needToSetProfile && currentProfile.needToAgreeTerms) {
        this.termsModal.openModal();
      }

    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (!FormHelper.isFormValid(this.createProfileForm)) {
      return;
    }

    this.accountService.createProfile(this.createProfileRequest)
      .finally(() => this.submitted = false)
      .subscribe(() => {
        this.accountService.getCurrentProfile(true).subscribe((profile) => {
          if (profile.needToAgreeTerms) {
            this.termsModal.openModal();
          } else {
            const redirectUrl = RedirectHelper.getLoginFlowRedirectUrl(profile);
            this.router.navigate([redirectUrl]);
          }
        });
    });
  }

}
