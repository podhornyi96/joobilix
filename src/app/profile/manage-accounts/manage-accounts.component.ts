import {Component, OnInit} from '@angular/core';
import {OrganizationService} from '../organization.service';
import {AccountSummaryModel} from '../accounts-table/account-summary.model';
import {PositionsTableData} from '../../positions/positions-table/models/positions-table-data';
import {PositionSummary} from '../../positions/positions-table/models/position-summary';
import {AccountTableDataModel} from '../accounts-table/account-table-data.model';
import {LoaderService} from '../../shared/loader/loader.service';

@Component({
  selector: 'jblx-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.less']
})
export class ManageAccountsComponent implements OnInit {

  accountsTableData: AccountTableDataModel;

  constructor(private organizationService: OrganizationService, private spinner: LoaderService) {
  }

  ngOnInit() {
    this.spinner.start();

    this.organizationService.getAccounts().subscribe((data: AccountSummaryModel[]) => {
      this.accountsTableData = new AccountTableDataModel();
      this.accountsTableData.accounts = data;

      this.spinner.complete();
    });
  }


  initPositionsTableData(accounts: AccountSummaryModel[]) {
    this.accountsTableData = new AccountTableDataModel();
    this.accountsTableData.accounts = accounts;
  }

}
