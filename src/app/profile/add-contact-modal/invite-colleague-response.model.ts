import {InviteColleagueStatusCode} from './invite-colleague.model';

export class InviteColleagueResponseModel {
  errorCode: InviteColleagueStatusCode;
}
