import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {InviteColleagueModel} from './invite-colleague.model';
import {OrganizationService} from '../organization.service';
import {NgForm} from '@angular/forms';
import {FormHelper} from '../../shared/helpers/formHelper';
import {InviteColleagueResponseModel} from './invite-colleague-response.model';
import {Location} from '@angular/common';

@Component({
  selector: 'jblx-add-contact-modal',
  templateUrl: './add-contact-modal.component.html',
  styleUrls: ['./add-contact-modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AddContactModalComponent implements OnInit, OnChanges {

  @ViewChild('content') modal: ElementRef;

  @Input() redirectAfterSave: boolean;

  modalReference: NgbModalRef;

  inviteColleagueModel: InviteColleagueModel = new InviteColleagueModel();

  submitted = false;

  errorCode: number;

  string = String;

  constructor(private modalService: NgbModal, private organizationService: OrganizationService, private location: Location) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    this.errorCode = 0;
  }

  show(): void {
    this.modalReference = this.modalService.open(this.modal, {size: 'lg', backdrop: 'static'});
  }

  onSubmit(form: NgForm): void {
    this.submitted = true;

    if (!FormHelper.isFormValid(form)) {
      return;
    }

    this.errorCode = 0;

    this.organizationService.invite(this.inviteColleagueModel).subscribe((resp: InviteColleagueResponseModel) => {
      if (resp.errorCode === 0) {
        form.reset();
        this.closeModal();
        return;
      }

      this.errorCode = resp.errorCode;
    });
  }

  closeModal(): void {
    this.submitted = false;
    this.modalReference.close();

    if (this.redirectAfterSave) {
      this.location.back();
    }
  }

}
