export class InviteColleagueModel {
  firstName: string;
  lastName: string;
  emailAddress: string;
}

export enum InviteColleagueStatusCode {
  'No Error' = 0,
  'User already exist' = 1,
  'Couldn\'t invite user call support' = 2
}
