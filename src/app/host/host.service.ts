import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HostProfileDataResponse} from '../shared/models/host/profile/host-profile-data-response.model';
import {HostEventsResponse} from '../shared/models/host/event/host-events-response.model';
import {HostEventCompanyPageResponseModel} from '../shared/models/host/company/host-event-company-page-response.model';
import {HostCompanyResponseModel} from '../shared/models/host/company/host-company-response.model';
import {HostCompanyPageResponseModel} from '../shared/models/host/company/host-company-page-response.model';
import {UpdateProfileRequest} from '../shared/models/host/profile/update-profile-request.model';
import {HostAddCompanyToEventRequestModel} from '../shared/models/host/company/host-add-company-to-event-request.model';
import {HostCompanyAutoCompleteModel} from '../shared/models/host/company/host-company-auto-complete.model';
import {DeleteCompanyFromEventRequest} from '../shared/models/host/company/host-delete-company-from-event-request.model';
import {HostEventCompanyDetailsModel} from '../shared/models/host/company/host-event-company-details.model';
import {HostEventRequest} from '../home/host-event-modal/host-event-request.model';
import {HostEventResponse} from '../home/host-event-modal/host-event-response.model';
import {environment} from '../../environments/environment';

@Injectable()
export class HostService {

  constructor(private http: HttpClient) {
  }

  getHostProfile(): Observable<HostProfileDataResponse> {
    return this.http.get<HostProfileDataResponse>(`${environment.apiUrl}/api/v2/Host/Profile`);
  }

  getHostEvents(skip: number, limit: number): Observable<HostEventsResponse> {
    return this.http.get<HostEventsResponse>(`${environment.apiUrl}/api/v2/Events`, {
      params: {
        skip: skip.toString(),
        limit: limit.toString()
      }
    });
  }

  updateProfile(input: UpdateProfileRequest): Observable<HostProfileDataResponse> {
    return this.http.put<HostProfileDataResponse>(`${environment.apiUrl}/api/v2/Host/Profile`, input);
  }

  getHostCompanies(skip: number, limit: number): Observable<HostCompanyPageResponseModel> {
    return this.http.get<HostCompanyPageResponseModel>(`${environment.apiUrl}/api/v2/Host/Companies`, {
      params: {
        skip: skip.toString(),
        limit: limit.toString()
      }
    });
  }

  getCompanyById(companyId: string): Observable<HostCompanyResponseModel> {
    return this.http.get<HostCompanyResponseModel>(`${environment.apiUrl}/api/v2/Host/Companies/${companyId}`);
  }

  getEventCompanies(skip: number, limit: number, eventId: string): Observable<HostEventCompanyPageResponseModel> {
    return this.http.get<HostEventCompanyPageResponseModel>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Companies`, {
      params: {
        skip: skip.toString(),
        limit: limit.toString()
      }
    });
  }

  createEventCompany(input: HostAddCompanyToEventRequestModel, eventId: string): Observable<any> {
    return this.http.post<number>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Company`, input);
  }

  eventCompanyAutocomplete(eventId: string): Observable<HostCompanyAutoCompleteModel[]> {
    return this.http
      .get<HostCompanyAutoCompleteModel[]>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Companies/Autocomplete`);
  }

  deleteCompanyFromEvent(input: DeleteCompanyFromEventRequest, eventId: string) {
    return this.http.post<number>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Disable`, input);
  }

  getEventCompany(eventId: string, companyId: string): Observable<HostEventCompanyDetailsModel> {
    return this.http
      .get<HostEventCompanyDetailsModel>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Companies/${companyId}`);
  }

  sendEventRemainder(eventId: string): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/api/v2/Host/Events/${eventId}/Remainder`, {});
  }

  hostEvent(hostEvent: HostEventRequest): Observable<HostEventResponse> {
    return this.http.post<HostEventResponse>(`${environment.apiUrl}/public/v1/Landing/Event`, hostEvent);
  }
}
